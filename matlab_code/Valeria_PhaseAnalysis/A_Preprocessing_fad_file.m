clear all; close all;

Folderpath='L:\Somnus-Data\Data01\BMS\adults\data\EGI\BMS_001\session2\sleep\'; % folderpath of raw data
fad_file = 'L:\Somnus-Data\User\Valeria_Jaramillo\Data\Stimulation_Analysis\BMS_adults\fadfiles\BMS_001_1_fad.mat' % type in location and name for saving

%% read in raw files
currentfolder=cd;
directory = uigetdir(Folderpath,'select Folder containing .raw files');
cd(directory)
[file_name] = uigetfile('*.raw', 'Select the first .raw file');
filename=file_name(4:end-4);

numfile=input('HOW MANY .RAW FILES DO YOU WANT TO PROCESS? ');
% visname=input('Type name of vis file as string: ');

vispath=[directory,'\'];
datapath=[directory,'\'];
cd(currentfolder);


dataFormat='int16';

% Filter
locutoff = 0.5;
hicutoff = 40;

fsraw = 500;

scordat =[];
restdata=[];
ffttot=[];
for i=1:numfile
    clear data datar datax 
    scorblock=[];
    
    if i<10
        numfilstr=['0' num2str(i)];
    else
        numfilstr=num2str(i);
    end
    
    filenameraw=[datapath,numfilstr,'_',filename,'.raw'];
    fid = fopen(filenameraw,'rb','b')
    [segInfo, dataFormat, header_array, EventCodes,Samp_Rate, NChan, scale, NSamp, NEvent] = readRAWFileHeader(fid);
    fclose(fid);
    newsamp = NSamp;
    fsraw=Samp_Rate;

   
     %     f3  f4  c3  c4 o1 o2 a1  a2  eog1  eog2   emg(change if necessary!!!!)
%     ndxch=[24 124 36 104 70 83 57 100 128 1 125 32 49 56];
    ndxch=[24 124 36 104 70 83 57 100 128 1 125 32 107 113];
    %      1   2   3  4  5  6   7  8   9 10 11  12 13 14

    
    for ch= [1,8,13,14]
        datax = loadEGIBigRaw(filenameraw,ndxch(ch));
        dataf=eegfilt(datax,fsraw,0,hicutoff);
        dataf2=eegfilt(dataf,fsraw,locutoff,0);
        datar(ch,:)= downsample(dataf2,5);
    end     
    
    fsfad = 100;
    
    f3a2=(datar(1,:)-datar(8,:));
  %  f4a1=(datar(2,:)-datar(7,:));
  %  c3a2=(datar(3,:)-datar(8,:));
  %  c4a1=(datar(4,:)-datar(7,:));
  %  o1a2=(datar(5,:)-datar(8,:));
  %  o2a1=(datar(6,:)-datar(7,:));
  %  eog1=(datar(9,:)-datar(10,:));
  %  eog2=(datar(11,:)-datar(12,:));
    emg=(datar(13,:)-datar(14,:));

   % eog1f=eegfilt(eog1,fsfad,0,40);
   % eog2f=eegfilt(eog2,fsfad,0,40);
    emgf=eegfilt(emg,fsfad,20,40);
    f3a2f = eegfilt(f3a2,fsfad,20,40);

    scorblock=[f3a2;emgf;f3a2f];
    
    scordat=[scordat scorblock];
    
end

f3a2 = scordat(1,:)';
emgf = scordat(2,:)';
f3a2f = scordat(3,:)';


 save(fad_file, 'f3a2','emgf','f3a2f');

