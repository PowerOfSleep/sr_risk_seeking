% calculate rms thresholds for 3 bands: delta (0.5- 4 Hz), alpha (8-13 Hz),
% beta (13- 40 Hz) based on filtered and downsampled data

clear all; close all;

% load fad file
fad_file_dir = dir('L:\Somnus-Data\User\Valeria_Jaramillo\Data\Stimulation_Analysis\BMS_adults\base\*.mat');

save_file = 'D:\Matlab\Stimulation_Analysis\Final_algorithm\thresholds_base_art_cor.mat';

subjects_base = cell(0);
th_delta_rms_40_base = [];
th_alpha_rms_75_base = [];
th_beta_rms_75_base = [];

for k = 1: length(fad_file_dir)
    
    fad_file = fad_file_dir(k).name;
    subject = fad_file(1:end-8);
    display(subject);

    load(['L:\Somnus-Data\User\Valeria_Jaramillo\Data\Stimulation_Analysis\BMS_adults\base\' ,fad_file]);

    clear th_alpha_rms_75
    clear th_beta_rms_75
    clear th_delta_rms_40
    %% resort into epoches 
    fsfad = 100;
    t_ep = 5*60; % time per epoche [s]
    n_ep = t_ep*fsfad; % # samples per epoche [-]
    N_ep = floor(size(f3a2,1)/n_ep); % # of epoches whole night [-]

    f3a2_ep = NaN(n_ep,N_ep);
    for i=1:N_ep
        f3a2_ep(:,i) = f3a2((i-1)*n_ep + 1 : i*n_ep);
    end

    %% filter using fft
    f3a2_delta = filter_with_fft(f3a2_ep, 0.5, 4, fsfad);
    f3a2_alpha = filter_with_fft(f3a2_ep, 8, 13, fsfad);
    f3a2_beta = filter_with_fft(f3a2_ep, 13, 40, fsfad);
    
    %% rms per epoche with histogram
    f3a2_delta_rms = rms(f3a2_delta,1);
    % f3a2_delta_rms_std = 3 * std(f3a2_delta_rms);
    % f3a2_delta_rms(f3a2_delta_rms > f3a2_delta_rms_std) = NaN;
    th_delta_rms_40 = prctile(f3a2_delta_rms,[40],2);

    f3a2_alpha_rms = rms(f3a2_alpha,1);
    % f3a2_alpha_rms_std = 3 * std(f3a2_alpha_rms);
    % f3a2_alpha_rms(f3a2_alpha_rms > f3a2_alpha_rms_std) = NaN;
    th_alpha_rms_75 = prctile(f3a2_alpha_rms,[75],2);

    f3a2_beta_rms = rms(f3a2_beta,1);
    % f3a2_beta_rms_std = 3 * std(f3a2_beta_rms);
    % f3a2_beta_rms(f3a2_beta_rms > f3a2_beta_rms_std) = NaN;
    th_beta_rms_75 = prctile(f3a2_beta_rms,[75],2);
    
%     figure
%     hist(f3a2_delta_rms,15)
%     title('F3 referenced to A2')
%     hold on;
%     x1 = [th_delta_rms_40 th_delta_rms_40];
%     y = [0 20];
%     plot (x1,y,'r');
%     
%     figure
%     hist(f3a2_alpha_rms,15)
%     title('F3 referenced to A2')
%     hold on;
%     x2 = [th_alpha_rms_75 th_alpha_rms_75];
%     plot (x2,y,'r');
% 
%     figure
%     hist(f3a2_beta_rms,15)
%     title('F3 referenced to A2')
%     hold on;
%     x3 = [th_beta_rms_75 th_beta_rms_75];
%     y = [0 20];
%     plot (x3,y,'r');
%     xlabel('rms for 5 min epochs with FFT');
%     ylabel('Counts'); 
    
    subjects_base = vertcat(subjects_base ,subject);
    th_delta_rms_40_base(end+1) = th_delta_rms_40;
    th_alpha_rms_75_base(end+1) = th_alpha_rms_75;
    th_beta_rms_75_base(end+1) = th_beta_rms_75;


end

th_delta_base_mean = mean(th_delta_rms_40_base);
th_alpha_base_mean = mean(th_alpha_rms_75_base);
th_beta_base_mean = mean(th_beta_rms_75_base);

save(save_file,'subjects_base','th_delta_rms_40_base','th_alpha_rms_75_base', 'th_beta_rms_75_base','th_delta_base_mean','th_alpha_base_mean','th_beta_base_mean');

