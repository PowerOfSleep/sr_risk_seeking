%% Create structure for separate electrode phase triggers (negative and positive half wave)

clear all;
close all;

study={'SWS_TMS' 'SWS_TMS_Control' 'BMS' 'SWS_MTM' 'SWS_TMS_1' 'SWS_TMS_2'};

load 'D:\SWS_Chord_PN\data\Phase_trigs_allSubs_org.mat'

for st=3:length(study)
    
    if st==1
        sub={'ST07MS' 'ST17GW' 'ST19EH' 'ST20FG' 'ST26DK' 'ST30NH' 'ST31LE' 'ST34DS' 'ST40FD' 'ST45CS' 'ST52CB' 'ST54TP' 'ST59GA'};
        datapath='L:\Somnus-Data\Data01\SWS_TMS\EGI\sleep\Filtered and analysed files\Filtered_Data\SWS_TMS_mainExperiment\down\';
    elseif st==2
        sub={'ST60PF' 'ST66PH' 'ST69EP' 'ST70MS' 'ST73SZ' 'ST77NP' 'ST80MS'};
        datapath='L:\Somnus-Data\Data01\SWS_TMS\EGI\sleep\Filtered and analysed files\Filtered_Data\SWS_TMS_controlExperiment\down\';
    elseif st==3
        sub={'BMS_001\' 'BMS_002\' 'BMS_004\' 'BMS_005\' 'BMS_008\' 'BMS_012\' 'BMS_022\' 'BMS_026\' 'BMS_028\' 'BMS_029\' 'BMS_031\' 'BMS_032\' 'BMS_033\' 'BMS_034\' 'BMS_035\' 'BMS_036\'};
        datapath='L:\Somnus-Data\User\Georgia_Sousouri\Data\Filtered_Data\BMS\down\';
    elseif st==4
        sub={'SWS_001_AG\' 'SWS_002_PH\' 'SWS_004_MH\' 'SWS_005_RA\' 'SWS_010_MK\' 'SWS_011_OR\' 'SWS_012_RS\' 'SWS_013_FE\' 'SWS_014_TW\' 'SWS_016_PH\' 'SWS_018_MS\'};
        datapath='L:\Somnus-Data\Data01\SWS_MTM\EGI\sleep\Filtered and analysed files\Filtered_Data\down\';
    elseif st==5 
        sub={'ST02BL' 'ST05TS' 'ST09RE' 'ST13DK' 'ST21JF' 'ST25BU'};
        datapath='L:\Somnus-Data\Data01\SWS_TMS\EGI\sleep\Filtered and analysed files\Filtered_Data\SWS_TMS_firstStudy\SWS_TMS_1\down\';
    elseif st==6
        sub={'ST02BL' 'ST05TS' 'ST09RE' 'ST13DK' 'ST21JF' 'ST25BU'};
        datapath='L:\Somnus-Data\Data01\SWS_TMS\EGI\sleep\Filtered and analysed files\Filtered_Data\SWS_TMS_firstStudy\SWS_TMS_2\down\';
    end

    savefolder=['D:\SWS_Chord_PN\data\Phase\down\',study{st},'\'];
    mkdir(savefolder)

    swData=dir([datapath,'*FiltFiltData_allnight.mat']);
    tPosData=dir([datapath,'*_FiltData_allnight.mat']);

    phase_ep_st=[270 300 330 0 30 60 90 120 150 180 210 240];
    phase_ep_end=[299 329 359 29 59 89 119 149 179 209 239 269];

for s=11:length(sub)
    
%     matObj = matfile([datapath,swData(s).name]);
%     da = matObj.data_SW(1,:);
    
    load([datapath,swData(s).name],'data_SW','StimCh')
    load([datapath,tPosData(s).name],'tPos_allCor','tPos_allCor_goodndx')
         
%     hilb = hilbert(data_SW');
%     %X=fft(hilb(:,1));
%     sigphase = angle(hilb);
%     sigphase_degree=(sigphase+pi)./pi.*180;
    
    tPos_allCor_badndx=setdiff(find(tPos_allCor),tPos_allCor_goodndx); % indices of triggers outside of N2/N3
    tPos_allCor_good=tPos_allCor(tPos_allCor_goodndx)+9; % delay of EEG data inherent in the anti-alias filters of EGI amplifiers (18ms)
    tPos_allCor_bad=tPos_allCor(tPos_allCor_badndx)+9;
    
    eval([sub{s},'=struct([]);'])
        
    for i=1:size(data_SW,1)
        display(study{st})
        display(sub{s})
        display(['chan ',num2str(i)])
        display('.......')
        
        hilb = hilbert(data_SW(i,:)');
        %X=fft(hilb(:,1));
        sigphase = angle(hilb);
        sigphase_degree=(sigphase+pi)./pi.*180;
        
        Nep_good_ndx=cell(1,12); Nep_bad_ndx=cell(1,12);
        ep_phase_good=cell(1,12); ep_phase_bad=cell(1,12); 
        
        % Find phase of triggers in N2/N3
        for t=1:length(tPos_allCor_good)
            
            if round(sigphase_degree(tPos_allCor_good(t)))==360
                currentphase=0;
            else
                currentphase=round(sigphase_degree(tPos_allCor_good(t)));
            end
            
            for e=1:length(phase_ep_st)
                
                if ismember(currentphase,(phase_ep_st(e):phase_ep_end(e)));
                    eval(['Nep_good_ndx{1,',num2str(e),'}=[Nep_good_ndx{1,',num2str(e),'} t];']) % indices of triggers (from tPos_allCor(tPos_allCor_goodndx)+9) in each bin
                    eval(['ep_phase_good{1,',num2str(e),'}=[ep_phase_good{1,',num2str(e),'} currentphase];'])
                end
            end
        end
        ntrig_neg_good=length(Nep_good_ndx{1,1})+length(Nep_good_ndx{1,2})+length(Nep_good_ndx{1,3})+...
                       length(Nep_good_ndx{1,4})+length(Nep_good_ndx{1,5})+length(Nep_good_ndx{1,6});
        ntrig_pos_good=length(tPos_allCor_good)-ntrig_neg_good;
        
        % Find phase of triggers outside N2/N3
        for j=1:length(tPos_allCor_bad)
            
            if round(sigphase_degree(tPos_allCor_bad(j)))==360
                currentphase=0;
            else
                currentphase=round(sigphase_degree(tPos_allCor_bad(j)));
            end
                
            for e=1:length(phase_ep_st)
                    
                if ismember(currentphase,(phase_ep_st(e):phase_ep_end(e)));
                    eval(['Nep_bad_ndx{1,',num2str(e),'}=[Nep_bad_ndx{1,',num2str(e),'} j];']) % indices of triggers (from tPos_allCor(tPos_allCor_goodndx)+9) in each bin
                    eval(['ep_phase_bad{1,',num2str(e),'}=[ep_phase_bad{1,',num2str(e),'} currentphase];'])
                end
            end
        end
        ntrig_neg_bad=length(Nep_bad_ndx{1,1})+length(Nep_bad_ndx{1,2})+length(Nep_bad_ndx{1,3})+...
                      length(Nep_bad_ndx{1,4})+length(Nep_bad_ndx{1,5})+length(Nep_bad_ndx{1,6});
        ntrig_pos_bad=length(tPos_allCor_bad)-ntrig_neg_bad;
        
        % Fill in the structure
        tdms=dir(['D:\SWS_Chord_PN\data\LabView\',study{st},'\*tdms.mat']);
        load(['D:\SWS_Chord_PN\data\LabView\',study{st},'\',tdms(s).name])
        clear tdms 
        
        data=Phase_trigs_allSubs_org;
        data.study=study{st};
        data.Subject=sub{s};
        data.channel=StimCh(i);
        if st == 5
            data.Stim_el=30;
        elseif st == 6
            data.Stim_el=105;
        end
        data.Stim_el=my_tdms_struct.g_1.c_0.Props.EEG_Channel;
        clear metaStruct my_tdms_struct
        data.Nep_good_ndx=Nep_good_ndx;
        data.Nep_bad_ndx=Nep_bad_ndx;
        data.ep_phase_good=ep_phase_good;
        data.ep_phase_bad=ep_phase_bad;
        data.ntrig_neg_good=ntrig_neg_good;
        data.ntrig_pos_good=ntrig_pos_good;
        data.ntrig_neg_bad=ntrig_neg_bad;
        data.ntrig_pos_bad=ntrig_pos_bad;
        
        eval([sub{s},'=([',sub{s},' data]);'])
        
        clear ntrig* Ntrig* mphase Nep* ep* data sigphase* hilb
    end
    eval(['save ',savefolder,sub{s},'.mat ',sub{s}])
    clear nTrig_all tPos_allCor* data_SW
end
clear e* i n N* phase* SC* swData tPosData s 
end

%% Separate triggers in up and down phase

clear all; close all;

study={'SWS_TMS' 'SWS_TMS_Control' 'BMS' 'SWS_MTM' 'SWS_TMS_1' 'SWS_TMS_2'};

for st=1:length(study)
    
    if st==1
        sub={'ST07MS' 'ST17GW' 'ST19EH' 'ST20FG' 'ST26DK' 'ST30NH' 'ST31LE' 'ST34DS' ...
             'ST40FD' 'ST45CS' 'ST52CB' 'ST54TP' 'ST59GA'};
    elseif st==2
        sub={'ST60PF' 'ST66PH' 'ST69EP' 'ST70MS' 'ST73SZ' 'ST77NP' 'ST80MS'};
    elseif st==3
        sub={'BMS_001' 'BMS_002' 'BMS_004' 'BMS_005' 'BMS_008' 'BMS_012' 'BMS_022' 'BMS_026' ...
             'BMS_028' 'BMS_029' 'BMS_032' 'BMS_033' 'BMS_034' 'BMS_035' 'BMS_036'};
    elseif st==4
        sub={'SWS_001_AG' 'SWS_002_PH' 'SWS_004_MH' 'SWS_005_RA' 'SWS_010_MK' ...
             'SWS_011_OR' 'SWS_013_FE' 'SWS_016_PH' 'SWS_018_MS'};
    elseif st==5 || st==6
        sub={'ST02BL' 'ST05TS' 'ST09RE' 'ST13DK' 'ST21JF' 'ST25BU'};
    end
    
    savefolder=['D:\SWS_Chord_PN\data\Phase\down\',study{st},'\'];
    
    for s=1:length(sub)
        
        load(['D:\SWS_Chord_PN\data\Phase\down\',study{st},'\',sub{s}])
        eval(['data=',sub{s},';'])
    
        for i=1:length(data)
                
            data(i).ntrig_down_good=length(data(i).Nep_good_ndx{1,1})+length(data(i).Nep_good_ndx{1,2})+length(data(i).Nep_good_ndx{1,3})+...
                        length(data(i).Nep_good_ndx{1,10})+length(data(i).Nep_good_ndx{1,11})+length(data(i).Nep_good_ndx{1,12});
            data(i).ntrig_up_good=length(data(i).Nep_good_ndx{1,4})+length(data(i).Nep_good_ndx{1,5})+length(data(i).Nep_good_ndx{1,6})+...
                        length(data(i).Nep_good_ndx{1,7})+length(data(i).Nep_good_ndx{1,8})+length(data(i).Nep_good_ndx{1,9});
                   
            data(i).ntrig_down_bad=length(data(i).Nep_bad_ndx{1,1})+length(data(i).Nep_bad_ndx{1,2})+length(data(i).Nep_bad_ndx{1,3})+...
                        length(data(i).Nep_bad_ndx{1,10})+length(data(i).Nep_bad_ndx{1,11})+length(data(i).Nep_bad_ndx{1,12});
            data(i).ntrig_up_bad=length(data(i).Nep_bad_ndx{1,4})+length(data(i).Nep_bad_ndx{1,5})+length(data(i).Nep_bad_ndx{1,6})+...
                        length(data(i).Nep_bad_ndx{1,7})+length(data(i).Nep_bad_ndx{1,8})+length(data(i).Nep_bad_ndx{1,9});
                   
        end
        eval([sub{s},'=data;'])
        save([savefolder,sub{s}],sub{s}) 
        clear data
    end
end

%% BMS (0.7-2 Hz)

sub={'BMS_001' 'BMS_002' 'BMS_004' 'BMS_005' 'BMS_008' 'BMS_012' 'BMS_022' 'BMS_026' ...
     'BMS_028' 'BMS_029' 'BMS_032' 'BMS_033' 'BMS_034' 'BMS_035' 'BMS_036'};

load('D:\SWS_Chord_PN\data\Ratios\All_Studies_filtChans\down\all_chans.mat')
load('D:\SWS_Chord_PN\data\Ratios\BMS\down\0.7-2 Hz\rel\ratios.mat')

inc=inc_ch{1,5}; 
dec=dec_ch{1,5};
    
eval(['data=',sub{1},';'])
    
inc_indx=[];
dec_indx=[];
for i=1:length(data)
        
    if ismember(data(i).channel,inc)
        inc_indx=[inc_indx i];
    elseif ismember(data(i).channel,dec)
        dec_indx=[dec_indx i];
    end
end
clear data

%----- Bar graphs triggers in negative or positive halfwave and in up-
%----- or down-going phase. (in and out of N2-N3)

props={'neg---pos','down---up'};

% inc_ch
ind=0;
for s=11:15
    
    eval(['data=',sub{s}])
    X=cell(1,length(inc_indx));

    for i=1:length(inc_indx)
    
        data1=data(inc_indx(i));
        X{1,i}=[data1.ntrig_neg_bad, data1.ntrig_pos_bad ; data1.ntrig_down_bad, data1.ntrig_up_bad];
    
        subplot(5,length(inc_indx),ind+i)
        bar(X{i})
        %ylim([0 1800])
        set(gca,'xticklabel',props)
        if s==11
            title(['chan.',num2str(data1.channel)],'FontSize',15)
        end
        if i==1
            ylabel(['Sub-',num2str(s)],'FontSize',15)
            ylh = get(gca,'ylabel');
            gyl = get(ylh);                                                         % Object Information
            ylp = get(ylh, 'Position');
            set(ylh, 'Rotation',0, 'Position',ylp, 'VerticalAlignment','middle', 'HorizontalAlignment','right')
        end
        hold on
        clear data1
    end
    ind=ind+length(inc_indx);
end

% dec_ch
ind=0;
for s=11:15
    
    eval(['data=',sub{s}])
    X=cell(1,length(dec_indx));

    for i=1:length(dec_indx)
    
        data1=data(dec_indx(i));
        X{1,i}=[data1.ntrig_neg_bad, data1.ntrig_pos_bad ; data1.ntrig_down_bad, data1.ntrig_up_bad];
    
        subplot(5,length(dec_indx),ind+i)
        bar(X{i})
        %ylim([0 1800])
        set(gca,'xticklabel',props)
        if s==11
            title(['chan.',num2str(data1.channel)],'FontSize',15)
        end
        if i==1
            ylabel(['Sub-',num2str(s)],'FontSize',15)
            ylh = get(gca,'ylabel');
            gyl = get(ylh);                                                         % Object Information
            ylp = get(ylh, 'Position');
            set(ylh, 'Rotation',0, 'Position',ylp, 'VerticalAlignment','middle', 'HorizontalAlignment','right')
        end
        hold on
        clear data1
    end
    ind=ind+length(dec_indx);
end

%----- Bar graphs triggers in each phase bin of the whole wave

props={'1','2','3','4','5','6','7','8','9','10','11','12'};

% inc_ch
ind=0;
for s=11:15
    
    eval(['data=',sub{s}])
    X=cell(1,length(inc_indx));

    for i=1:length(inc_indx)
    
        data1=data(inc_indx(i));
        X{1,i}=[length(data1.Nep_good_ndx{1}), length(data1.Nep_good_ndx{2}), length(data1.Nep_good_ndx{3}), length(data1.Nep_good_ndx{4}),...
            length(data1.Nep_good_ndx{5}),length(data1.Nep_good_ndx{6}),length(data1.Nep_good_ndx{7}),length(data1.Nep_good_ndx{8}),...
            length(data1.Nep_good_ndx{9}),length(data1.Nep_good_ndx{10}),length(data1.Nep_good_ndx{11}),length(data1.Nep_good_ndx{12})];
    
        subplot(5,length(inc_indx),ind+i)
        bar(X{i})
        %ylim([0 1800])
        set(gca,'xticklabel',props)
        if s==11
            title(['chan.',num2str(data1.channel)],'FontSize',15)
        end
        if i==1
            ylabel(['Sub-',num2str(s)],'FontSize',15)
            ylh = get(gca,'ylabel');
            gyl = get(ylh);                                                         % Object Information
            ylp = get(ylh, 'Position');
            set(ylh, 'Rotation',0, 'Position',ylp, 'VerticalAlignment','middle', 'HorizontalAlignment','right')
        end
        hold on
        clear data1
    end
    ind=ind+length(inc_indx);
end

% dec_ch
ind=0;
for s=11:15
    
    eval(['data=',sub{s}])
    X=cell(1,length(dec_indx));

    for i=1:length(dec_indx)
    
        data1=data(dec_indx(i));
        X{1,i}=[length(data1.Nep_good_ndx{1}), length(data1.Nep_good_ndx{2}), length(data1.Nep_good_ndx{3}), length(data1.Nep_good_ndx{4}),...
            length(data1.Nep_good_ndx{5}),length(data1.Nep_good_ndx{6}),length(data1.Nep_good_ndx{7}),length(data1.Nep_good_ndx{8}),...
            length(data1.Nep_good_ndx{9}),length(data1.Nep_good_ndx{10}),length(data1.Nep_good_ndx{11}),length(data1.Nep_good_ndx{12})];
    
        subplot(5,length(dec_indx),ind+i)
        bar(X{i})
        %ylim([0 1800])
        set(gca,'xticklabel',props)
        if s==11
            title(['chan.',num2str(data1.channel)],'FontSize',15)
        end
        if i==1
            ylabel(['Sub-',num2str(s)],'FontSize',15)
            ylh = get(gca,'ylabel');
            gyl = get(ylh);                                                         % Object Information
            ylp = get(ylh, 'Position');
            set(ylh, 'Rotation',0, 'Position',ylp, 'VerticalAlignment','middle', 'HorizontalAlignment','right')
        end
        hold on
        clear data1
    end
    ind=ind+length(dec_indx);
end

x=(0:0.1:2*pi);
plot(sin(x))
hold on
for i=1:11
plot([10*i*pi/6 10*i*pi/6],[-1 1])
end
hline(0)



























