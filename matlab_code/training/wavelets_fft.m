%% Training and example Code
%
%
%
%
%
%
%
%
%

%% Wavelet generation

% parameters necessary to generate a wavelet
srate = 500; % sampling rate in Hz
f     = 10; % frequency of wavelet in Hz
time  = -1:1/srate:1; % time, from -1 to 1 second in steps of 1/sampling-rate
s     = 6/(2*pi*f);

% and together they make a wavelet
wavelet = exp(2*pi*1i*f.*time) .* exp(-time.^2./(2*s^2)); 

% plotting for quick check

figure

subplot(221)
% show the projection onto the real axis
plot3(time,real(wavelet),imag(wavelet),'m')
xlabel('Time (ms)'), ylabel('real axis')
view(0,90)
title('Projection onto real and time axes')

% show the projection onto the imaginary axis
subplot(222)
plot3(time,real(wavelet),imag(wavelet),'g')
xlabel('Time (ms)'), ylabel('imaginary axis')
view(0,0)
title('Projection onto imaginary and time axes') 
 
% plot projection onto real and imaginary axes
subplot(223)
plot3(time,real(wavelet),imag(wavelet),'k')
ylabel('real axis'), zlabel('imag axis')
view(90,0)
title('Projection onto imaginary and time axes')

% plot real and imaginary projections simultaneously
subplot(224)
plot(time,real(wavelet),'b')
hold on
plot(time,imag(wavelet),'b:')
legend({'real part';'imaginary part'})

% now show in 3d
figure

plot3(time,real(wavelet),imag(wavelet),'k')
xlabel('Time (ms)'), ylabel('real amplitude'), zlabel('imag amplitude')
title('3-D projection (click and spin with mouse)')
axis equal
rotate3d

%% FFT of a wavelet and EEG data

% create wavelet
frequency = 6; % in Hz, as usual
time = -1:1/EEG.srate:1;
s    = (4/(2*pi*frequency))^2; % note that s is squared here rather than in the next line...
wavelet = exp(2*1i*pi*frequency.*time) .* exp(-time.^2./(2*s)/frequency);

% FFT parameters
n_wavelet            = length(wavelet);
n_data               = EEG.pnts;
n_convolution        = n_wavelet+n_data-1;
half_of_wavelet_size = (length(wavelet)-1)/2;

% FFT of wavelet and EEG data
% To make this work you need to preload EEG.data or an equivalent 

fft_wavelet = fft(wavelet,n_convolution);
fft_data    = fft(squeeze(EEG.data(47,:,1)),n_convolution); % FCz, trial 1

convolution_result_fft = ifft(fft_wavelet.*fft_data,n_convolution) * sqrt(s);

% cut off edges
convolution_result_fft = convolution_result_fft(half_of_wavelet_size+1:end-half_of_wavelet_size);

% plot for comparison
figure
subplot(311)
plot(EEG.times,real(convolution_result_fft))
xlabel('Time (ms)'), ylabel('Voltage (\muV)')
title([ 'Projection onto real axis is filtered signal at ' num2str(frequency) ' Hz.' ])

subplot(312)
plot(EEG.times,abs(convolution_result_fft).^2)
xlabel('Time (ms)'), ylabel('Power (\muV^2)')
title([ 'Magnitude of projection vector squared is power at ' num2str(frequency) ' Hz.' ])

subplot(313)
plot(EEG.times,angle(convolution_result_fft))
xlabel('Time (ms)'), ylabel('Phase angle (rad.)')
title([ 'Angle of vector is phase angle time series at ' num2str(frequency) ' Hz.' ])







































