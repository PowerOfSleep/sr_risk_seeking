clear; close all
Session={'stim'};

%Folder='D:\Daten\SWS\SWS_TMS\EGI\Analysen\newStudy\stim\';
Folder='F:\Daten\SWS\SWS_TMS\SWS_TMS_2\EGI\stim\'; %% SWS_TMS_2
excludedchannels=[43 48 63 68 73 81 88 94 99 107 113 119 120 125 126 127 128];



for l=1:length(Session)
     Folderpath=[Folder];
     SubSess=dir([Folderpath,'*FFT.mat']);
    
     
      for s=7:length(SubSess)
          load([Folderpath,SubSess(s).name],'vissymb','art*','tPos_allCor')
         
      

            
            
             if exist('artndxn_cor')
                    clear artndxn
                     artndxn=artndxn_cor;
                     clear artndxn_cor
              end
                
               indexnr=find(vissymb=='2' | vissymb=='3' | vissymb=='4')';

           
                artndxn(excludedchannels,:)=0;

                ndxgoodch=find(sum(artndxn')>0);
                nchgood=length(ndxgoodch);

                artndxx=find(sum(artndxn(ndxgoodch,:))==nchgood);               %%%%artefacts rejection in all channel same
                channelndx=intersect(artndxx,indexnr);

                NREMSec=[];
                
                for ii=1:length(channelndx)
                    NREMSec=[NREMSec [((channelndx(ii)-1)*20)+1:1:channelndx(ii)*20]];
                end
                
                tPos_allSec=round(tPos_allCor/500);
                
                [tPos_goodSec, tPos_allCor_goodndx]=intersect(tPos_allSec,NREMSec);
                
                save([Folderpath,SubSess(s).name],'tPos_allCor_goodndx','-append')
                clear t* a* n* vissymb index* channelndx NREMSec i ii j
      end
end
                
                
                
                
                