clear;
close all;


%% Filtfilt
datapath_FFT='D:\User\Sara\AmpServer\Studies\SWS_TMS\SW_Spindels\stim\';
datapath='D:\User\Sara\AmpServer\Studies\SWS_TMS\FilterData_FiltFilt\stim\';

swData=dir([datapath,'*FiltDataFiltFilt_minCh.mat']);
FFTData=dir([datapath_FFT,'*FFT.mat']);

phase_ep_st=[270 300 330 0 30 60];
phase_ep_end=[299 329 359 29 59 89];



for s=1:size(swData,1)
    
    if sum(FFTData(s).name(1:8)==swData(s).name(1:8))~=8
        error('wrong sub matching')
    end
    
    %load([datapath,swData(s).name],'data_SW')
    load([datapath,swData(s).name],'data_SW_min')
    data_SW=data_SW_min; clear data_SW_min
    load([datapath_FFT,FFTData(s).name],'tPos_allCor','tPos_allCor_goodndx')
    
   
    
    hilb = hilbert(data_SW);
    sigphase = angle(hilb);
    sigphase_degree=(sigphase+pi)./pi.*180;
    
    j=1; trig_pos=0;
    Nep_1=0; Nep_2=0; Nep_3=0; Nep_4=0; Nep_5=0; Nep_6=0;
    ep_1=[]; ep_2=[]; ep_3=[]; ep_4=[]; ep_5=[]; ep_6=[];
    
    tPos_allCor_good=tPos_allCor(tPos_allCor_goodndx)+9;
    
    
   % g=1;
    for t=1:length(tPos_allCor_good)
        
        if sigphase_degree(tPos_allCor_good(t))>=0 & sigphase_degree(tPos_allCor_good(t))<=89 | sigphase_degree(tPos_allCor_good(t))>=270 &   sigphase_degree(tPos_allCor_good(t))<=360 %data_SW(tPos_allCor(t))<0  
            if round(sigphase_degree(tPos_allCor_good(t)))==360
                currentphase=0;
            else
                currentphase=round(sigphase_degree(tPos_allCor_good(t)));
            end

            for e=1:length(phase_ep_st)
                if ismember(currentphase,[phase_ep_st(e):phase_ep_end(e)]);
                    eval(['Nep_',num2str(e),'=Nep_',num2str(e),'+1',';'])
                    eval(['ep_',num2str(e),'=[ep_',num2str(e),' currentphase]',';'])
                    
                    
%                 elseif  ~ismember(round(sigphase_degree(tPos_allCor(t))),[phase_ep_st(1):phase_ep_end(1)]) & ~ismember(round(sigphase_degree(tPos_allCor(t))),[phase_ep_st(2):phase_ep_end(2)])...
%                         & ~ismember(round(sigphase_degree(tPos_allCor(t))),[phase_ep_st(3):phase_ep_end(3)]) & ~ismember(round(sigphase_degree(tPos_allCor(t))),[phase_ep_st(4):phase_ep_end(4)])...
%                         & ~ismember(round(sigphase_degree(tPos_allCor(t))),[phase_ep_st(5):phase_ep_end(5)]) & ~ismember(round(sigphase_degree(tPos_allCor(t))),[phase_ep_st(6):phase_ep_end(6)])
%                          badtrig(g)=t; 
%                          g=g+1;
                end
            end
      
        else
            trig_pos=trig_pos+1;
        end
    end

    nTrig_all(s)=length(tPos_allCor_good);
    ntrig_neg(s)=length(tPos_allCor_good)-trig_pos;
    ntrig_pos(s)=trig_pos;
    
    nphase_e1(s)=Nep_1;
    nphase_e2(s)=Nep_2;
    nphase_e3(s)=Nep_3;
    nphase_e4(s)=Nep_4;
    nphase_e5(s)=Nep_5;
    nphase_e6(s)=Nep_6;
    
    mphase_e1(s)=mean(ep_1);
    mphase_e2(s)=mean(ep_2);
    mphase_e3(s)=mean(ep_3);
    mphase_e4(s)=mean(ep_4);
    mphase_e5(s)=mean(ep_5);
    mphase_e6(s)=mean(ep_6);
    
    clear ep_* Nep* trig_pos data_SW tPos_allCor sigphase* hilb
end
 
    
 excludsub=[1 6 11 13 15];

mphase_e1(excludsub)=[];
mphase_e2(excludsub)=[];
mphase_e3(excludsub)=[];
mphase_e4(excludsub)=[];
mphase_e5(excludsub)=[];
mphase_e6(excludsub)=[];

nphase_e1(excludsub)=[];
nphase_e2(excludsub)=[];
nphase_e3(excludsub)=[];
nphase_e4(excludsub)=[];
nphase_e5(excludsub)=[];
nphase_e6(excludsub)=[];

ntrig_pos(excludsub)=[];
ntrig_neg(excludsub)=[];
nTrig_all(excludsub)=[];

save('D:\User\Sara\AmpServer\Studies\SWS_TMS\FilterData_FiltFilt\Phase_of_Trig_Chmin_onlygoodTrig.mat','nphase_e*','ntrig_neg','nTrig_all','ntrig_pos','mp*')


%% 
%%%% check test
% clear; close all
% load('D:\User\Sara\AmpServer\Studies\SWS_TMS\FilterData_FiltFilt\Phase_of_Trig_Chmin_onlygoodTrig.mat')
% mphase_e1_2=mphase_e1; clear mphase_e1
% mphase_e2_2=mphase_e2; clear mphase_e2
% mphase_e3_2=mphase_e3; clear mphase_e3
% mphase_e4_2=mphase_e4; clear mphase_e4
% mphase_e5_2=mphase_e5; clear mphase_e5
% 
% nphase_e1_2=nphase_e1; clear nphase_e1
% nphase_e2_2=nphase_e2; clear nphase_e2
% nphase_e3_2=nphase_e3; clear nphase_e3
% nphase_e4_2=nphase_e4; clear nphase_e4
% nphase_e5_2=nphase_e5; clear nphase_e5
% 
% load('D:\User\Sara\AmpServer\Studies\SWS_TMS\FilterData_FiltFilt\test\Phase_of_Trig_Chmin_onlygoodTrig.mat')
% 
% 
% plot(nphase_e5_2-nphase_e5)

%%



