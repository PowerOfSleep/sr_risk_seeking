
clear all; close all;

%% single sub

% filename = 'F:\Daten\SWS\SWS_Epi\TDMS\SWEpi03_audio.tdms';
% 
% my_tdms_struct = TDMS_getStruct(filename);
% [~,metaStruct] = TDMS_readTDMSFile(filename);
% % 
% save([filename(1:end-5),'_tdms.mat'], 'metaStruct', 'my_tdms_struct')
% clear fliename Subname metaStruct my_tdms_struct





%% LS
% folderpath='E:\Local_SWS\TDMS\';
% 
% 
% SubSession=dir([folderpath,'\*.tdms']);
% for s=1:length(SubSession)
%     Subname=SubSession(s).name;
%     filename = [folderpath,Subname];
% 
%     my_tdms_struct = TDMS_getStruct(filename);
%     [~,metaStruct] = TDMS_readTDMSFile(filename);
% 
%     save([filename(1:end-5),'_tdms.mat'], 'metaStruct', 'my_tdms_struct')
%     clear fliename Subname metaStruct my_tdms_struct
% end
%   
        



%% SWS TMS
% folderpath='E:\SWS_TMS\TDMS\';
% 
% session={'Session2' 'Session3'};
% 
% for l=1:length(session)
%     SubSession=dir([folderpath,session{l},'\*.tdms']);
%     for s=1:length(SubSession)
%         Subname=SubSession(s).name;
%         filename = [folderpath,session{l},'\',Subname];
% 
%         my_tdms_struct = TDMS_getStruct(filename);
%         [~,metaStruct] = TDMS_readTDMSFile(filename);
% 
%         save([filename(1:end-5),'_tdms.mat'], 'metaStruct', 'my_tdms_struct')
%         clear fliename Subname metaStruct my_tdms_struct
%     end
%     clear SubSession
%         
% end

%%

%% SWS TMS 2

folderpath='D:\Niklas\TestData\SWAB(Pilot)\data\TDMS files\';
SubSession=dir([folderpath,'*.tdms']);
for s=1:length(SubSession)
    Subname=SubSession(s).name;
    filename = [folderpath,Subname];

    my_tdms_struct = TDMS_getStruct(filename);
    [~,metaStruct] = TDMS_readTDMSFile(filename);

    save([filename(1:end-5),'_tdms_test.mat'], 'metaStruct', 'my_tdms_struct')
    clear fliename Subname metaStruct my_tdms_struct
end



