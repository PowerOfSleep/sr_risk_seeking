
%% SWS_TMS_2
clear; close;

Folder=['F:\Daten\SWS\SWS_TMS\SWS_TMS_2\EGI\'];
Session={'stim\'};


for l=1:length(Session)
     SubSess_tPos=dir([Folder,Session{l},'*FiltData_allnight.mat']);
     Sub_FFT=dir([Folder,Session{l},'*FFT.mat']);
    
    for s=7:length(Sub_FFT)
        if sum(SubSess_tPos(s).name(1:8)==Sub_FFT(s).name(1:8))==8
            load([Folder,Session{l},SubSess_tPos(s).name],'tPos_allCor')
            save([Folder,Session{l},Sub_FFT(s).name],'tPos_allCor','-append')
            clear tPos_allCor
        else
            error('wrong sub match')
        end
    end        
end