
clear;close all;
fs=500;
refCh=[49 56];
trigch=130;


%%% Filter SW
Fc1=[0.319825 3.12648]/(fs/2); %fs=500 
N1=3;
[z1,p1,k1] = butter(N1, Fc1);
[sos_SW,g_SW] = zp2sos(z1, p1, k1);


%%
Folderpath='I:\Sara\Studies\SWS_Epi\Data\EGI\';
subfolder=dir([Folderpath,'SWEpi*']);


for s=1:length(subfolder)
    display(s)
    Rawname=dir([Folderpath,subfolder(s).name,'\*.raw']);
    FFTname=dir([Folderpath,subfolder(s).name,'\*_FFT.mat']);
    load([Folderpath,subfolder(s).name,'\',FFTname(1).name],'StimCh')
    numfile=length(Rawname);
    data_filt=[]; 
    
    if sum(FFTname.name(1:7)== Rawname(s).name(4:10))~=7
        error('wrong sub matching')
    end
    
    for i=1:numfile
         
        display(i)
        filenameraw=[Folderpath,subfolder(s).name,'\',Rawname(i).name];
        data_ref1 = loadEGIBigRaw(filenameraw,49);
        data_ref2 = loadEGIBigRaw(filenameraw,56);
        data_ch = loadEGIBigRaw(filenameraw,StimCh);
        data_ref_ch=data_ch-((data_ref1+data_ref2)/2);
        data_ref_ch_filt=filtfilt(sos_SW,g_SW,data_ref_ch);
        
        clear data_ref_ch data_ch data_ref2 data_ref1 filenameraw
        
        data_filt=[data_filt data_ref_ch_filt];
        clear data_ref_ch_filt
    end
    
    save([Folderpath,subfolder(s).name,'\',subfolder(s).name,'_FiltData_allnight_FiltFilt.mat'],'data_filt','StimCh','fs','refCh','sos_SW','g_SW')
    
    clear numfile data_filt StimCh FFTname Rawname
end
                    

