
%%% Daten von Trigger Kanal neu referenzieren (links und rechts ohr) und
%%% als zusammenhängende Variable abspeichern


%% SWS_TMS_2 %%%% Struktur anpassen für SWS_TMS 2

clear;close 
fs=500;
%refCh=[49 56];
%trigch=130;

refCh='average reference';

trigch=117;
%%% Filter
Fc1=[0.319825 3.12648]/(fs/2); %fs=500
N1=3;
[z1,p1,k1] = butter(N1, Fc1);
[sos_var1,g1] = zp2sos(z1, p1, k1);
Hd_but1 = dfilt.df2sos(sos_var1, g1);


%%
Session={'session2\' 'session3\'};

%sub={'ST34DS' 'ST37AM' 'ST40FD' 'ST43MA' 'ST45CS' 'ST49SH' 'ST52CB' 'ST54TP' 'ST59GA'}; % 
sub={'SWAB'}
Folder='D:\Niklas\TestData\SWAB(Pilot)\test\stim\longmat_average_ref\';

Folder_raw='D:\Niklas\TestData\SWAB(Pilot)\data\EGI\Netstation and raw files\SWAB_KP1\Session1\';

FFT_File=dir([Folder,'*FFT.mat']);    

saveFolder=Folder;

for s=7:length(sub)

     if sum(FFT_File(s).name(1:6)==sub{s})==6
         loadfile=[Folder,FFT_File(s).name];
         load(loadfile,'StimCh');
             
         for l=1:length(Session)
             Rawname=dir([Folder_raw,sub{s},'\',Session{l},'*.raw']); 
             numfile=length(Rawname);
             tPos_all=[]; data=[]; samp=0;
             

                for i=1:numfile
         
                    display(i)
                    filenameraw=[Folder_raw,sub{s},'\',Session{l},Rawname(i).name];

                    data_ref1 = loadEGIBigRaw(filenameraw,49);
                    data_ref2 = loadEGIBigRaw(filenameraw,56);
                    currentsamp=length(data_ref1);
                    
                   
                    data_ch = loadEGIBigRaw(filenameraw,StimCh);  
                    data_ref=data_ch-((data_ref1+data_ref2)/2);
                    data_ref_fil=filter(Hd_but1,data_ref);
                    clear data_ch data_ref
                    
                    data=[data data_ref_fil];
                    clear data_ref_fil
                    
                    
                    if sum(FFT_File(s).name(1:8)==Rawname(1).name(4:11))==8
                         eval('tPos=egiGetTriggersCorr(filenameraw,trigch);','tPos=[]') 
                         tPos_row=tPos+samp;
                         tPos_all=[tPos_all,tPos_row];
                         samp=samp+currentsamp;
                         clear tPos tPos_row
                    end
                    clear data_ref1 data_ref2 filenameraw currentsamp
                    
                end
                
               if ~isempty(tPos_all)
                    save([saveFolder,Rawname(1).name(4:11),'_FiltData_allnight.mat'],'data','StimCh','fs','refCh','Hd_but1','tPos_all')
               else
                 save([saveFolder,Rawname(1).name(4:11),'_FiltData_allnight.mat'],'data','StimCh','fs','refCh','Hd_but1')
               end
               
               clear Rawname numfile  tPos_all data samp    
         end
         
     else
             error('wrong Sub matching') 
     end
     clear StimCh
end
         


%%











