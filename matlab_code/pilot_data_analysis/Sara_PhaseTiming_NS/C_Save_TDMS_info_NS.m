%% C_Save_TDMS_info_NS

% Adds variables 'StimCh', 'NStim_LV' from the TDMS files to the FFT.mat files in the 'Folder_FFT'...

clear; close all;

%% 

% Folder to TDMS files, should be the same as in 'A_ToDo_ReadTDMSFiles_NS'
Folder_TDMS='C:\Studium\PhD\Data\TDMS files\';

% Folder to FFT files, should be the same as in 'B_Save_FFT_File_NS'
Folder_FFT='C:\Studium\PhD\Data\test\stim\';

% Get file directories including all *tdms.mat and *FFT.mat files
TDMS_File=dir([Folder_TDMS,'*tdms.mat']);
FFT_File=dir([Folder_FFT,'*FFT.mat']);

% Includes 'StimCh' and 'NStim_LV' to the FFT files...

 for s=1:length(TDMS_File)
     
     load([Folder_TDMS,TDMS_File(s).name])
     
    if sum(FFT_File(s).name(1:6)==TDMS_File(s).name(1:6))==6
         savename=[Folder_FFT,FFT_File(s).name];

          StimCh = my_tdms_struct.g_1.c_0.Props.EEG_Channel;
          NStim_LV = length(metaStruct.numberDataPoints)-2;

          save(savename, 'StimCh', 'NStim_LV', '-append')
          clear StimCh NStim_LV my_tdms_struct metaStruct savename loadfile
    end
 end
