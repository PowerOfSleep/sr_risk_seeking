
clear; close all;

%% LS
% folderpath='E:\Local_SWS\TDMS\';
% 
% 
% SubSession=dir([folderpath,'\*.tdms']);
% for s=1:length(SubSession)
%     Subname=SubSession(s).name;
%     filename = [folderpath,Subname];
% 
%     my_tdms_struct = TDMS_getStruct(filename);
%     [~,metaStruct] = TDMS_readTDMSFile(filename);
% 
%     save([filename(1:end-5),'_tdms.mat'], 'metaStruct', 'my_tdms_struct')
%     clear fliename Subname metaStruct my_tdms_struct
% end
%   
        



%% SWS TMS
% folderpath='E:\SWS_TMS\TDMS\';
% 
% session={'Session2' 'Session3'};
% 
% for l=1:length(session)
%     SubSession=dir([folderpath,session{l},'\*.tdms']);
%     for s=1:length(SubSession)
%         Subname=SubSession(s).name;
%         filename = [folderpath,session{l},'\',Subname];
% 
%         my_tdms_struct = TDMS_getStruct(filename);
%         [~,metaStruct] = TDMS_readTDMSFile(filename);
% 
%         save([filename(1:end-5),'_tdms.mat'], 'metaStruct', 'my_tdms_struct')
%         clear fliename Subname metaStruct my_tdms_struct
%     end
%     clear SubSession
%         
% end
%% SWS EPI
% filename='F:\Daten\SWS\SWS_Epi\TDMS\SWEpi22_audio.tdms';
% 
% 
% my_tdms_struct = TDMS_getStruct(filename);
% [~,metaStruct] = TDMS_readTDMSFile(filename);
% 
% save([filename(1:end-5),'_tdms.mat'], 'metaStruct', 'my_tdms_struct')
% clear fliename Subname metaStruct my_tdms_struct

%% SWS MTM

% folderpath='G:\SWS_MTM\data\TDMS\included';
% % 
% 
% SubSession=dir([folderpath,'\*.tdms']);
%     for s=1:length(SubSession)
%         Subname=SubSession(s).name;
%         filename = [folderpath,'\',Subname];
% 
%         my_tdms_struct = TDMS_getStruct(filename);
%         [~,metaStruct] = TDMS_readTDMSFile(filename);
% 
%         save([filename(1:end-5),'_tdms.mat'], 'metaStruct', 'my_tdms_struct')
%         clear fliename Subname metaStruct my_tdms_struct
%     end

%% SWS TMS 2
% folderpath='L:\Somnus-Data\Studies\SWS_TMS_2\data\TDMS\';
% 
% %session={'Session2' 'Session3'};
% 
% 
% SubSession=dir([folderpath,'*.tdms']);
% for s=1:length(SubSession)
%     Subname=SubSession(s).name;
%     filename = [folderpath,Subname];
% 
%     my_tdms_struct = TDMS_getStruct(filename);
%     [~,metaStruct] = TDMS_readTDMSFile(filename);
% 
%     save([filename(1:end-5),'_tdms.mat'], 'metaStruct', 'my_tdms_struct')
%     clear fliename Subname metaStruct my_tdms_struct
% end

%% single File

filename='F:\Daten\SWS\SWS_Epi\TDMS\ip\SWEpi56_audio.tdms'
my_tdms_struct = TDMS_getStruct(filename);
[~,metaStruct] = TDMS_readTDMSFile(filename);
 
save([filename(1:end-5),'_tdms.mat'], 'metaStruct', 'my_tdms_struct')

%%  File Loop

% folder='F:\Daten\SWS\tests\test_PIB_SWS_Epi_13_7_16\tdms\neu\'
% files=dir([folder,'*.tdms']);
% for i=1:length(files)
%     filename=[folder,files(i).name];
%     my_tdms_struct = TDMS_getStruct(filename);
%     [~,metaStruct] = TDMS_readTDMSFile(filename);
% 
%     save([filename(1:end-5),'_tdms.mat'], 'metaStruct', 'my_tdms_struct')
%     clear filename
% end
