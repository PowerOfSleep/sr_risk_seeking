%% A_ToDo_ReadTDMSFiles_NS

% Reads TDMS files and saves them in a .mat file with the ending
% 'save([filename(1:end-5),'_tdms.mat']'

%%

clear; close all;

% Path to TDMS data; TDMS data of multiple subject can be stored within the
% same folder
folderpath='C:\Studium\PhD\Data\TDMS files\';

% Access all tdms files within the folder
SubSession=dir([folderpath,'*.tdms']);

for s=1:length(SubSession)
    Subname=SubSession(s).name;
    filename = [folderpath,Subname];

    my_tdms_struct = TDMS_getStruct(filename);
    [~,metaStruct] = TDMS_readTDMSFile(filename);

    save([filename(1:end-5),'_tdms.mat'], 'metaStruct', 'my_tdms_struct')
    clear fliename Subname metaStruct my_tdms_struct
    
end


%% single sub

% filename = 'F:\Daten\SWS\SWS_Epi\TDMS\SWEpi03_audio.tdms';
% 
% my_tdms_struct = TDMS_getStruct(filename);
% [~,metaStruct] = TDMS_readTDMSFile(filename);
% % 
% save([filename(1:end-5),'_tdms.mat'], 'metaStruct', 'my_tdms_struct')
% clear fliename Subname metaStruct my_tdms_struct





%% LS
% folderpath='E:\Local_SWS\TDMS\';
% 
% 
% SubSession=dir([folderpath,'\*.tdms']);
% for s=1:length(SubSession)
%     Subname=SubSession(s).name;
%     filename = [folderpath,Subname];
% 
%     my_tdms_struct = TDMS_getStruct(filename);
%     [~,metaStruct] = TDMS_readTDMSFile(filename);
% 
%     save([filename(1:end-5),'_tdms.mat'], 'metaStruct', 'my_tdms_struct')
%     clear fliename Subname metaStruct my_tdms_struct
% end
%   
        



%% SWS TMS
% folderpath='E:\SWS_TMS\TDMS\';
% 
% session={'Session2' 'Session3'};
% 
% for l=1:length(session)
%     SubSession=dir([folderpath,session{l},'\*.tdms']);
%     for s=1:length(SubSession)
%         Subname=SubSession(s).name;
%         filename = [folderpath,session{l},'\',Subname];
% 
%         my_tdms_struct = TDMS_getStruct(filename);
%         [~,metaStruct] = TDMS_readTDMSFile(filename);
% 
%         save([filename(1:end-5),'_tdms.mat'], 'metaStruct', 'my_tdms_struct')
%         clear fliename Subname metaStruct my_tdms_struct
%     end
%     clear SubSession
%         
% end
