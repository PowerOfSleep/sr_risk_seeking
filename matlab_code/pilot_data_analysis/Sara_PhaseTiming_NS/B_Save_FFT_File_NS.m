%% B_Save_FFT_File_NS

% Creates a *_FFT.mat file per subject that contains the following
% variables obtained from the longmat files:
% 'nch','numfile','ffttot','artndxn','fs','vissymb','reference','Name_longMatFile'

%%

clear; close all;

% folder to longmat files 
Folder='C:\Studium\PhD\Data\test\';

% depending on directory structure the Session might be bs = baseline or stim = stimulation; 
Session={'bs\' 'stim\'};

% goes through all *.mat files in the folder and extracts and saves
% 'nch','numfile','ffttot','artndxn','fs','vissymb','reference','Name_longMatFile'
% in a new *_FFT.mat file

% To access both sessions at once (bs and stim) copy the for loop and
% change 'Session{x}' to 'Session{y}'

for l=1:length(Session)
    
     SubSess=dir([Folder,Session{2},'\*.mat']);
    
    for s=1:length(SubSess)
        SubPath=[Folder,Session{2},SubSess(s).name];
        load(SubPath)
        long_MF=(SubPath);
        Name_longMatFile=long_MF(end-31:end);
        save([Folder,Session{2},Name_longMatFile(7:14),'_FFT.mat'],'nch','numfile','ffttot','artndxn','fs','vissymb','reference','Name_longMatFile')
        clear MatFile Name_longMatFile VisFile artndxn ffttot fs nch numfile vissymb SubPath long_MF reference
    end
    
end



%% SWS_MTM
% sub=dir('E:\User_Sara\AmpServer\Data\SWS_MTM\EGI\SWS*');
% Session={'session1\' 'session2\'};
% 
% for s=1:3
%     display(s)
%     
%     for l=1:2 %% session loop
%         Folder=['E:\User_Sara\AmpServer\Data\SWS_MTM\EGI\',sub(s).name,'\',Session{l},'sleep\Analysis\'];
%         MatFile=dir([Folder,'*mat']);
%         VisFile=dir([Folder,'*vis']);
%         load([Folder,MatFile.name])
%         Name_longMatFile={MatFile.name};
%         save([Folder,VisFile.name(1:end-4),'_FFT.mat'],'nch','numfile','ffttot','artndxn','fs','vissymb','reference','Name_longMatFile')
%         clear Folder MatFile Name_longMatFile VisFile artndxn ffttot fs nch numfile vissymb
%     end
% end

%% LS

% sub=dir('E:\User_Sara\AmpServer\Studies\Local_SWS\Data\EGI\LS*');
% Session={'session1\' 'session2\'};
% 
% for s=1:length(sub)
%     display(s)
%     
%     for l=1:2 %% session loop
%         Folder=['E:\User_Sara\AmpServer\Studies\Local_SWS\Data\EGI\',sub(s).name,'\',Session{l},'Analysis\'];
%         MatFile=dir([Folder,'*mat']);
%         VisFile=dir([Folder,'*vis']);
%         load([Folder,MatFile.name])
%         Name_longMatFile={MatFile.name};
%         save([Folder,VisFile.name(1:end-4),'_FFT.mat'],'nch','numfile','ffttot','artndxn','fs','vissymb','reference','Name_longMatFile')
%         clear Folder MatFile Name_longMatFile VisFile artndxn ffttot fs nch numfile vissymb
%     end
% end


%% SWS_TMS

% Folder=['D:\Daten\SWS\SWS_TMS\EGI\Analysen\'];
% Session={'session2\' 'session3\'};
% for l=1:length(Session)
%     
%      SubSess=dir([Folder,Session{l},'newStudy\*.mat']);
%     
%     for s=1:length(SubSess)
%         SubPath=[Folder,Session{l},'newStudy\',SubSess(s).name];
%         load(SubPath)
%         long_MF=(SubPath);
%         Name_longMatFile=long_MF(53:end);
%         save([Folder,Session{l},'newStudy\',Name_longMatFile(7:14),'_FFT.mat'],'nch','numfile','ffttot','artndxn','fs','vissymb','reference','Name_longMatFile')
%         clear MatFile Name_longMatFile VisFile artndxn ffttot fs nch numfile vissymb SubPath long_MF reference
%     end
% end

%% SWS_TMS_2

% Folder=['F:\Daten\SWS\SWS_TMS\SWS_TMS_2\EGI\'];
% Session={'bs\' 'stim\'};
% for l=1:length(Session)
%     
%      SubSess=dir([Folder,Session{l},'ip\*.mat']);
%     
%     for s=1:length(SubSess)
%         SubPath=[Folder,Session{l},'ip\',SubSess(s).name];
%         load(SubPath)
%         long_MF=(SubPath);
%         Name_longMatFile=long_MF(end-29:end);
%         save([Folder,Session{l},'ip\',Name_longMatFile(7:14),'_FFT.mat'],'nch','numfile','ffttot','artndxn','fs','vissymb','reference','Name_longMatFile')
%         clear MatFile Name_longMatFile VisFile artndxn ffttot fs nch numfile vissymb SubPath long_MF reference
%     end
% end

