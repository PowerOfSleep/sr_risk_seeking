
clear; close all
%% ESES

%Sub='SWEpi56'
Sub='SWEpi78'


%%
Folderpath='I:\Sara\Studies\SWS_Epi\Data\EGI\';
fs=500;

phase_ep_st=[270 300 330 0 30 60 90 120 150 180 210 240];
phase_ep_end=[299 329 359 29 59 89 119 149 179 209 239 269];

FFTFile=[Folderpath,Sub,'\',Sub,'_FFT.mat'];
FiltFiltDataFile=[Folderpath,Sub,'\',Sub,'_FiltData_allnight_FiltFilt.mat'];
load(FFTFile,'start*','end*','ndx_good_block','tPos_allCor_adj','tPos_allCor_adj_goodndx')
load(FiltFiltDataFile,'data_filt')
 
hilb = hilbert(data_filt);
sigphase = angle(hilb);
sigphase_degree=(sigphase+pi)./pi.*180;
    
ndx_good_down=ndx_good_block(:,2);


%%%% down blocks
block_phases(length(start_down),12)=zeros;

for b=1:length(start_down)
    
    if ndx_good_down(b)==1
        stimblock=[start_down(b):end_down(b)];
        stimblock_good=intersect(stimblock,tPos_allCor_adj_goodndx);
        tPos_allCor_good_block=tPos_allCor_adj(stimblock_good)+9;

        for t=1:length(tPos_allCor_good_block)
            
            if round(sigphase_degree(tPos_allCor_good_block(t)))==360
                currentphase=0;
            else
                currentphase=round(sigphase_degree(tPos_allCor_good_block(t)));
            end
            
            for e=1:length(phase_ep_st)
                if ismember(currentphase,[phase_ep_st(e):phase_ep_end(e)]);
                    block_phases(b,e)=block_phases(b,e)+1;
                end
            end
            clear currentphase
        end
        
        clear stimblock stimblock_good tPos_allCor_good_block
    end
end

save([Folderpath,Sub,'\',Sub,'_block_phases.mat'],'phase_ep_end','phase_ep_st','block_phases','ndx_good_down','ndx_good_block')





        
    
            
                
                
                
                
                
                    
                
 
         
    
    
    
    
    
   
    

