%% phase_forNegTrig_neu_NS

% (1) This script needs definetly a new name! For easier comparison with
% the original script by Sara the name will not be changed within the next
% couple of days.

% (2) This script lists the # of triggers of each phase according to the
% boundaries set in 'phase_ep_st' and 'phase_ep_end'

clear;
close all;


%% Filtfilt
datapath_FFT='C:\Studium\PhD\Data\test\tPos_allCor\';
datapath='C:\Studium\PhD\Data\test\tPos_allCor\';

swData=dir([datapath,'*FiltData_allnight_FiltFilt.mat']);
FFTData=dir([datapath_FFT,'*FFT.mat']);

phase_ep_st=[270 300 330 0 30 60 90 120 150 180 210 240];
phase_ep_end=[299 329 359 29 59 89 119 149 179 209 239 269];


for s=1:size(swData,1)
    
    if sum(FFTData(s).name(1:8)==swData(s).name(1:8))~=8
        error('wrong sub matching')
    end
    
    %load([datapath,swData(s).name],'data_SW')
    load([datapath,swData(s).name],'data_filt')
    data_SW=data_filt; clear data_filt
    load([datapath_FFT,FFTData(s).name],'tPos_allCor','tPos_allCor_goodndx')
    
    
    
    hilb = hilbert(data_SW);
    sigphase = angle(hilb);
    sigphase_degree=(sigphase+pi)./pi.*180;
    
    j=1; trig_pos=0;
    Nep_1=0; Nep_2=0; Nep_3=0; Nep_4=0; Nep_5=0; Nep_6=0; Nep_7=0; Nep_8=0; Nep_9=0; Nep_10=0; Nep_11=0; Nep_12=0;
    ep_1=[]; ep_2=[]; ep_3=[]; ep_4=[]; ep_5=[]; ep_6=[]; ep_7=[]; ep_8=[]; ep_9=[]; ep_10=[]; ep_11=[]; ep_12=[];
    
    tPos_allCor_good=tPos_allCor(tPos_allCor_goodndx)+9;
    
    
    % g=1;
    for t=1:length(tPos_allCor_good)
        
        if sigphase_degree(tPos_allCor_good(t))>=0 && sigphase_degree(tPos_allCor_good(t))<=360
            
            if round(sigphase_degree(tPos_allCor_good(t)))==360
                currentphase=0;
            else
                currentphase=round(sigphase_degree(tPos_allCor_good(t)));
            end
            
            for e=1:length(phase_ep_st)
                if ismember(currentphase,[phase_ep_st(e):phase_ep_end(e)]);
                    eval(['Nep_',num2str(e),'=Nep_',num2str(e),'+1',';'])
                    eval(['ep_',num2str(e),'=[ep_',num2str(e),' currentphase]',';'])
                    
                    
                    %                 elseif  ~ismember(round(sigphase_degree(tPos_allCor(t))),[phase_ep_st(1):phase_ep_end(1)]) & ~ismember(round(sigphase_degree(tPos_allCor(t))),[phase_ep_st(2):phase_ep_end(2)])...
                    %                         & ~ismember(round(sigphase_degree(tPos_allCor(t))),[phase_ep_st(3):phase_ep_end(3)]) & ~ismember(round(sigphase_degree(tPos_allCor(t))),[phase_ep_st(4):phase_ep_end(4)])...
                    %                         & ~ismember(round(sigphase_degree(tPos_allCor(t))),[phase_ep_st(5):phase_ep_end(5)]) & ~ismember(round(sigphase_degree(tPos_allCor(t))),[phase_ep_st(6):phase_ep_end(6)])
                    %                          badtrig(g)=t;
                    %                          g=g+1;
                end
            end
            
        else
            trig_pos=trig_pos+1;
        end
    end
    
    nTrig_all(s)=length(tPos_allCor_good);
    ntrig_neg(s)=length(tPos_allCor_good)-trig_pos;
    ntrig_pos(s)=trig_pos;
    
    nphase_e1(s)=Nep_1;
    nphase_e2(s)=Nep_2;
    nphase_e3(s)=Nep_3;
    nphase_e4(s)=Nep_4;
    nphase_e5(s)=Nep_5;
    nphase_e6(s)=Nep_6;
    nphase_e7(s)=Nep_7;
    nphase_e8(s)=Nep_8;
    nphase_e9(s)=Nep_9;
    nphase_e10(s)=Nep_10;
    nphase_e11(s)=Nep_11;
    nphase_e12(s)=Nep_12;
    
    mphase_e1(s)=mean(ep_1);
    mphase_e2(s)=mean(ep_2);
    mphase_e3(s)=mean(ep_3);
    mphase_e4(s)=mean(ep_4);
    mphase_e5(s)=mean(ep_5);
    mphase_e6(s)=mean(ep_6);
    mphase_e7(s)=mean(ep_7);
    mphase_e8(s)=mean(ep_8);
    mphase_e9(s)=mean(ep_9);
    mphase_e10(s)=mean(ep_10);
    mphase_e11(s)=mean(ep_11);
    mphase_e12(s)=mean(ep_12);
    
    clear ep_* Nep* trig_pos data_SW tPos_allCor sigphase* hilb
end

% ?
excludsub=[1 6 11 13 15];

mphase_e1(excludsub)=[];
mphase_e2(excludsub)=[];
mphase_e3(excludsub)=[];
mphase_e4(excludsub)=[];
mphase_e5(excludsub)=[];
mphase_e6(excludsub)=[];
mphase_e7(excludsub)=[];
mphase_e8(excludsub)=[];
mphase_e9(excludsub)=[];
mphase_e10(excludsub)=[];
mphase_e11(excludsub)=[];
mphase_e12(excludsub)=[];

nphase_e1(excludsub)=[];
nphase_e2(excludsub)=[];
nphase_e3(excludsub)=[];
nphase_e4(excludsub)=[];
nphase_e5(excludsub)=[];
nphase_e6(excludsub)=[];
nphase_e7(excludsub)=[];
nphase_e8(excludsub)=[];
nphase_e9(excludsub)=[];
nphase_e10(excludsub)=[];
nphase_e11(excludsub)=[];
nphase_e12(excludsub)=[];

ntrig_pos(excludsub)=[];
ntrig_neg(excludsub)=[];
nTrig_all(excludsub)=[];

save([datapath_FFT,swData(s).name(1:end-4),'_Phase_of_Trig_onlygoodTrig.mat'],'nphase_e*','ntrig_neg','nTrig_all','ntrig_pos','mp*')

%%
%%%% check test
% clear; close all
% load('D:\User\Sara\AmpServer\Studies\SWS_TMS\FilterData_FiltFilt\Phase_of_Trig_Chmin_onlygoodTrig.mat')
% mphase_e1_2=mphase_e1; clear mphase_e1
% mphase_e2_2=mphase_e2; clear mphase_e2
% mphase_e3_2=mphase_e3; clear mphase_e3
% mphase_e4_2=mphase_e4; clear mphase_e4
% mphase_e5_2=mphase_e5; clear mphase_e5
%
% nphase_e1_2=nphase_e1; clear nphase_e1
% nphase_e2_2=nphase_e2; clear nphase_e2
% nphase_e3_2=nphase_e3; clear nphase_e3
% nphase_e4_2=nphase_e4; clear nphase_e4
% nphase_e5_2=nphase_e5; clear nphase_e5
%
% load('D:\User\Sara\AmpServer\Studies\SWS_TMS\FilterData_FiltFilt\test\Phase_of_Trig_Chmin_onlygoodTrig.mat')
%
%
% plot(nphase_e5_2-nphase_e5)

%%



