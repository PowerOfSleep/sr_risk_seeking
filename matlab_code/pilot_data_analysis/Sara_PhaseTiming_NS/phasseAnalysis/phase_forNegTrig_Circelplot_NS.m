clear;
close all;


%% SWSTMS Main Experiment
% datapath_FFT='D:\User\Sara\AmpServer\Studies\SWS_TMS\SW_Spindels\stim\';
% datapath='D:\User\Sara\AmpServer\Studies\SWS_TMS\FilterData_FiltFilt\stim\';
% 
% swData=dir([datapath,'*FiltDataFiltFilt_minCh.mat']);
% FFTData=dir([datapath_FFT,'*FFT.mat']);
% phase_tPos_rad_all_TMS_main=[];
% 
% excludsub=[1 6 11 13 15];
% 
% for s=1:size(swData,1)
%     
%     if ~ismember(s,excludsub)
%     
%     
%         if sum(FFTData(s).name(1:8)==swData(s).name(1:8))~=8
%             error('wrong sub matching')
%         end
% 
%         %load([datapath,swData(s).name],'data_SW')
%         load([datapath,swData(s).name],'data_SW_min')
%         data_SW=data_SW_min; clear data_SW_min
%         load([datapath_FFT,FFTData(s).name],'tPos_allCor','tPos_allCor_goodndx')
%         tPos_allCor_good=tPos_allCor(tPos_allCor_goodndx)+9;
% 
% 
%         hilb = hilbert(data_SW);
%         sigphase = angle(hilb);
%         sigphase_degree=(sigphase+pi)./pi.*180;
% 
% 
%         phase_tPos=sigphase_degree(tPos_allCor_good);
% 
%         phase_tPos_rad = circ_ang2rad(phase_tPos);       % convert to radians
%         phase_tPos_rad_all_TMS_main=[phase_tPos_rad_all_TMS_main phase_tPos_rad];
%         clear phase_tPos_rad phase_tPos sigphase_degree sigphase hilb tPos_allCor_good tPos_allCor tPos_allCor_goodndx data_SW
%     end
%     
% end
%     
% save('F:\Daten\SWS\SWS_TMS\SWS_TMS_all\stim\Phase_ofgoodTrig_rad\Phase_goodtPoss_rad_all.mat','phase_tPos_rad_all_TMS_main')    
%     
% %sigphase_corr=sigphase+pi;
% %phase_tPos_rad=sigphase_corr(tPos_allCor_goodndx);
%         
% figure
% circ_plot(phase_tPos_rad_all_TMS_main','hist',[],20,true,true,'linewidth',2,'color','r')
        
 
  
%% SWSTMS Control Experiment

% datapath_FFT='F:\Daten\SWS\SWS_TMS\SWS_TMS_Control\EGI\Analysen\stim\';
% datapath='F:\Daten\SWS\SWS_TMS\SWS_TMS_Control\EGI\Analysen\stim\';
% 
% swData=dir([datapath,'*FiltData_allnight_TrigCh_FiltFilt.mat']);
% FFTData=dir([datapath_FFT,'*FFT.mat']);
% phase_tPos_rad_all_TMS_cont=[];
% 
% for s=1:size(swData,1)
%     
%     if sum(FFTData(s).name(1:8)==swData(s).name(1:8))~=8
%         error('wrong sub matching')
%     end
%     
%     %load([datapath,swData(s).name],'data_SW')
%     load([datapath,swData(s).name],'data')
%     load([datapath_FFT,FFTData(s).name],'tPos_allCor','tPos_allCor_goodndx')
%     tPos_allCor_good=tPos_allCor(tPos_allCor_goodndx)+9;
%    
%     
%     hilb = hilbert(data);
%     sigphase = angle(hilb);
%     sigphase_degree=(sigphase+pi)./pi.*180;
%     
%    
%     phase_tPos=sigphase_degree(tPos_allCor_good);
%   
%     phase_tPos_rad = circ_ang2rad(phase_tPos);       % convert to radians
%     phase_tPos_rad_all_TMS_cont=[phase_tPos_rad_all_TMS_cont phase_tPos_rad];
%     clear phase_tPos_rad phase_tPos sigphase_degree sigphase hilb tPos_allCor_good tPos_allCor tPos_allCor_goodndx data_SW data
% end
%     
% save('F:\Daten\SWS\SWS_TMS\SWS_TMS_Control\EGI\Analysen\stim\Phase_ofgoodTrig_rad\Phase_goodtPoss_rad_all.mat','phase_tPos_rad_all_TMS_cont')    
%     
% %sigphase_corr=sigphase+pi;
% %phase_tPos_rad=sigphase_corr(tPos_allCor_goodndx);
%         
% figure
% circ_plot(phase_tPos_rad_all_TMS_cont','hist',[],20,true,true,'linewidth',2,'color','r')


%% SWSTMS Control Experiment

% datapath_FFT='G:\SWS_MTM\data\EGI\Analyse\stim\Phasetiming_included\';
% datapath='G:\SWS_MTM\data\EGI\Analyse\stim\Phasetiming_included\';
% 
% swData=dir([datapath,'*FilterTrigCh_allNight_FiltFilt.mat']);
% FFTData=dir([datapath_FFT,'*FFT.mat']);
% phase_tPos_rad_all_MTM=[];
% 
% for s=1:size(swData,1)
%     
%     if sum(FFTData(s).name(1:8)==swData(s).name(1:8))~=8
%         error('wrong sub matching')
%     end
%     
%     %load([datapath,swData(s).name],'data_SW')
%     load([datapath,swData(s).name],'data')
%     load([datapath_FFT,FFTData(s).name],'tPos_allCor','tPos_allCor_goodndx')
%     tPos_allCor_good=tPos_allCor(tPos_allCor_goodndx)+9;
%    
%     
%     hilb = hilbert(data);
%     sigphase = angle(hilb);
%     sigphase_degree=(sigphase+pi)./pi.*180;
%     
%    
%     phase_tPos=sigphase_degree(tPos_allCor_good);
%   
%     phase_tPos_rad = circ_ang2rad(phase_tPos);       % convert to radians
%     phase_tPos_rad_all_MTM=[phase_tPos_rad_all_MTM phase_tPos_rad];
%     clear phase_tPos_rad phase_tPos sigphase_degree sigphase hilb tPos_allCor_good tPos_allCor tPos_allCor_goodndx data_SW data
% end
%     
% save('G:\SWS_MTM\data\EGI\Analyse\stim\Phasetiming_included\Phase_ofgoodTrig_rad\Phase_goodtPoss_rad_all.mat','phase_tPos_rad_all_MTM')    
%     
% %sigphase_corr=sigphase+pi;
% %phase_tPos_rad=sigphase_corr(tPos_allCor_goodndx);
%         
% figure
% circ_plot(phase_tPos_rad_all_MTM','hist',[],20,true,true,'linewidth',2,'color','r')    
    
    
%%

load('G:\SWS_MTM\data\EGI\Analyse\stim\Phasetiming_included\Phase_ofgoodTrig_rad\Phase_goodtPoss_rad_all.mat')
load('F:\Daten\SWS\SWS_TMS\SWS_TMS_all\stim\Phase_ofgoodTrig_rad\Phase_goodtPoss_rad_all.mat')
load('F:\Daten\SWS\SWS_TMS\SWS_TMS_Control\EGI\Analysen\stim\Phase_ofgoodTrig_rad\Phase_goodtPoss_rad_all.mat')

% figure
% subplot(3,1,1)
% circ_plot(phase_tPos_rad_all_TMS_main','hist',[],20,true,true,'linewidth',2,'color','r')    
% subplot(3,1,2)
% circ_plot(phase_tPos_rad_all_TMS_cont','hist',[],20,true,true,'linewidth',2,'color','r')  
% subplot(3,1,3)
% circ_plot(phase_tPos_rad_all_MTM','hist',[],20,true,true,'linewidth',2,'color','r')    

figure
circ_plot(phase_tPos_rad_all_TMS_main','hist',[],20,true,true,'linewidth',2,'color','r')    

    
    