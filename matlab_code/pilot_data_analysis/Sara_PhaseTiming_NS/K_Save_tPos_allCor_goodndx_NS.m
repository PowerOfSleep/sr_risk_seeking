%% K_Save_tPos_allCor_goodndx_NS

% Adds 'tPos_allCor_goodndx' to the '*_FFT.mat' files and saves the '*_FFT.mat' files
% in a new destination folder destdir=(Folder,'tPos_allCor\')

clear; close all

%%

Session={'stim'};

%Folder='D:\Daten\SWS\SWS_TMS\EGI\Analysen\newStudy\stim\';
Folder='C:\Studium\PhD\Data\test\stim\';

% Channels to be excluded by default because of their suboptimal position
% at the lower neck of the subject
excludedchannels=[43 48 63 68 73 81 88 94 99 107 113 119 120 125 126 127 128];

% Creates the new destinaltion directory; If it already exists the script
% still proceedes
mkdir (Folder,'tPos_allCor\');
destdir=([Folder,'tPos_allCor\']);

%%

for l=1:length(Session)
    Folderpath=[Folder];
    SubSess=dir([Folderpath,'*FFT.mat']);
    
    % Either use length(SubSess) to process all FFT files at once or use
    % 's=x:y' to exclude specific FFT files
    for s=2:5 %length(SubSess)
        load([Folderpath,SubSess(s).name],'vissymb','art*','tPos_allCor')
        
        if exist('artndxn_cor')
            clear artndxn
            artndxn=artndxn_cor;
            clear artndxn_cor
        end
        
        indexnr=find(vissymb=='2' | vissymb=='3' | vissymb=='4')';
        
        artndxn(excludedchannels,:)=0;
        
        ndxgoodch=find(sum(artndxn')>0);
        nchgood=length(ndxgoodch);
        
        %artefacts rejection in all channel same
        artndxx=find(sum(artndxn(ndxgoodch,:))==nchgood);
        channelndx=intersect(artndxx,indexnr);
        
        NREMSec=[];
        
        for ii=1:length(channelndx)
            NREMSec=[NREMSec [((channelndx(ii)-1)*20)+1:1:channelndx(ii)*20]];
        end
        
        tPos_allSec=round(tPos_allCor/500);
        
        [tPos_goodSec, tPos_allCor_goodndx]=intersect(tPos_allSec,NREMSec);
        
        save([destdir,SubSess(s).name],'tPos_allCor_goodndx','-append')
        clear t* a* n* vissymb index* channelndx NREMSec i ii j
    end
    
end
