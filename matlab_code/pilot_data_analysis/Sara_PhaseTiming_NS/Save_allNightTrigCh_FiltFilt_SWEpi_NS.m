%% Save_allNightTrigCh_FiltFilt_SWEpi_NS

% Re-references the 'StimCh' according to the 'refCh' in the *.raw files
% using filtfilt, saving them as '_FiltData_allnight_FiltFilt.mat'

clear;close all;

%% Sampling details and Filter

fs=500;
refCh=[49 56];
trigch=130;


% Filter SW
Fc1=[0.319825 3.12648]/(fs/2);
N1=3;
[z1,p1,k1] = butter(N1, Fc1);
[sos_SW,g_SW] = zp2sos(z1, p1, k1);


%% Folder structure

% Folder path to tPos_allCor created in 'K' 
Folderpath='D:\Niklas\TestData2\SWAB(Pilot)\data\EGI\Filtered and analysed files\long mat files\stim\tPos_allCor\';

% Folder path to the '*.raw' files
FolderpathRaw='D:\Niklas\TestData\SWAB(Pilot)\data\EGI\Netstation and raw files\';

% Subfolder structure of the raw files directory
SubFolder={'SWAB_KP1' 'SWAB_KP2' 'SWAB_KP3' 'SWAB_KP4' 'SWAB_KP5' 'SWAB_KP6'};

% Sessions within each Subfolder
Session={'bs' 'stim'};

%%

for s=1:length(SubFolder)
    display(s)
    Rawname=dir([FolderpathRaw,SubFolder{s},'\',Session{2},'\','\*.raw']);
    FFTname=dir([Folderpath,'\*_FFT.mat']);
    load([Folderpath,'\',FFTname(2).name],'StimCh')
    numfile=length(Rawname);
    data_filt=[];
    
    if sum(FFTname(s).name(1:8)== Rawname(s).name(4:11))~=8
        error('wrong sub matching')
    end
    
    for i=1:numfile
        
        display(i)
        filenameraw=[FolderpathRaw,SubFolder{s},'\',Session{2},'\',Rawname(i).name];
        data_ref1 = loadEGIBigRaw(filenameraw,49);
        data_ref2 = loadEGIBigRaw(filenameraw,56);
        data_ch = loadEGIBigRaw(filenameraw,StimCh);
        data_ref_ch=data_ch-((data_ref1+data_ref2)/2);
        data_ref_ch_filt=filtfilt(sos_SW,g_SW,data_ref_ch);
        
        clear data_ref_ch data_ch data_ref2 data_ref1 filenameraw
        
        data_filt=[data_filt data_ref_ch_filt];
        clear data_ref_ch_filt
    end
    
    save([Folderpath,SubFolder{s},'_FiltData_allnight_FiltFilt.mat'],'data_filt','StimCh','fs','refCh','sos_SW','g_SW')
    
    clear numfile data_filt StimCh FFTname Rawname
end


