%% E_get_TimeDelay_TDMS_GeoAdapted_savendxbadtrig_NS

clear; close all;

%% Save 'tPos_allCor' and 'ndx_badtrig'

% Other than the scripts before 'E' doesn't work for
% multiple TDMS/mat files at once. as this script checks differences
% between Flags saved in LabView and Flags saved in the NetStation files batch processing
% is not recommended. Also, 'E' allows direct visual examination of the
% Flag quality (see '% Quality plot'). 

% TDMS file
TDMS_File='D:\Niklas\TestData2\SWAB(Pilot)\data\TDMS files\SWAB_KP5_audio_1_tdms.mat';

% mat file generated in D:
Mat_File='D:\Niklas\TestData2\SWAB(Pilot)\data\EGI\Filtered and analysed files\long mat files\stim\SWAB_KP5_FiltData_allnight.mat';

% Boundaries for visual examination of Flag quality
gooddif=[-0.5 0 0.5];

load(TDMS_File)

for c=0:length(metaStruct.numberDataPoints)-3
     eval(['AmpSamp = my_tdms_struct.g_1.c_',num2str(c),'.Props.AMP_sample',';'])
     AmpSamp_al(c+1,:)=AmpSamp;
     clear AmpSamp
end

load(Mat_File,'tPos_all') 
tPos_all_org=tPos_all;

% Check for wrong flags and zeros 
AmpSamp_al=double(AmpSamp_al);

% indices of zeros
ndxzeros=find(AmpSamp_al==0);

% remove zeros from AmpSamp_al
AmpSamp_al(ndxzeros)=[];
nrAmpSamp_org=length(AmpSamp_al);

% indices of unique samples
[AmpSamp_al, ndxdoubel]=unique(AmpSamp_al);

% indices of double samples
doubel_stim=setdiff([1:nrAmpSamp_org],ndxdoubel);
nrWaveDif=length(tPos_all)-length(AmpSamp_al)
AmpSamp_500=AmpSamp_al./2;
endtPos_all=length(tPos_all);

Diff_ampSamp=diff(AmpSamp_500);
Diff_ampSamp=Diff_ampSamp';
Diff_tPos=diff(tPos_all);
ndx_badtrig=[];

if length(tPos_all)>=length(AmpSamp_al)

    w=1;
    g=1;
    i=1;
    j=0;
    enddif=length(Diff_ampSamp);
    difdif=Diff_ampSamp(w:enddif)-Diff_tPos(g:enddif+g-w);

        while ~ismember(difdif(1),gooddif)

            g=g+1;

                if enddif+g-w>length(Diff_tPos)
                    w=w+1;
                    g=1;
                end

                difdif=Diff_ampSamp(w:enddif)-Diff_tPos(g:enddif+g-w);
        end

        if w>1
            AmpSamp_500(1:w-1)=[];
            clear Diff_ampSamp
            Diff_ampSamp=diff(AmpSamp_500);
            Diff_ampSamp=Diff_ampSamp';
            enddif=length(Diff_ampSamp);
        end

        if g>1
            ndx_badtrig=(1:g-1);
            tPos_all(1:g-1)=[];
            clear Diff_tPos
            Diff_tPos=diff(tPos_all);
        end

            clear difdif
            difdif=Diff_ampSamp-Diff_tPos(1:enddif);  

        while length(tPos_all)>length(AmpSamp_al)

            while ismember(difdif(i),gooddif)
                i=i+1;

                if i>length(difdif)
                    break
                end

            end


            if i>length(difdif)
                clear difdif Diff_tPos
                if g>1
                    ndx_badtrig=[ndx_badtrig g+i+j:endtPos_all];
                else
                    ndx_badtrig=[ndx_badtrig i+j+1:endtPos_all];
                end
                tPos_all(i+1:end)=[]; 

                Diff_tPos=diff(tPos_all);
                difdif=Diff_ampSamp-Diff_tPos;            
            else

                clear difdif Diff_tPos
                if g>1
                    ndx_badtrig=[ndx_badtrig g+i+j];
                else
                    ndx_badtrig=[ndx_badtrig i+j+1];
                end
                j=j+1;
                tPos_all(i+1)=[];

                Diff_tPos=diff(tPos_all);
                difdif=Diff_ampSamp-Diff_tPos(1:enddif);
            end
        end
        
        % Quality plot
        plot(difdif);
        tPos_allCor=tPos_all;
else
    DISP('more flags in LV')
end
   
if ~exist('ndx_badtrig')
    ndx_badtrig=[];
end

save(Mat_File,'tPos_allCor','ndx_badtrig','-append')
        
