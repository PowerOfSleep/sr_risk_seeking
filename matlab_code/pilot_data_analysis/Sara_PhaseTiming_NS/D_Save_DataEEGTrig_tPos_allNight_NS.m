%% D_Save_DataEEGTrig_tPos_allNight_NS

% Daten von Trigger Kanal neu referenzieren (links und rechts ohr) und
% als zusammenhängende Variable abspeichern.


%% Filter and predefined Variables

clear; close all; 
% Sampling frequency
fs=500;
% new reference channels
refCh=[49 56];
% channel # that saves the trigger in NetStation
trigch=130;

%%% Filter
Fc1=[0.319825 3.12648]/(fs/2);
N1=3;
[z1,p1,k1] = butter(N1, Fc1);
[sos_var1,g1] = zp2sos(z1, p1, k1);
Hd_but1 = dfilt.df2sos(sos_var1, g1);


%% Access Data and save Data as one variable

% Folder to EGI filtered data. Note that this script handles *.mat files in
% a different order than *.raw files: EGIData/session, there is no
% additional folder-order for the subject# as this information is already
% within the *.mat file name.
Folder='C:\Studium\PhD\Data\test\stim\';

% Folder for *.raw data sets
Folder_raw='C:\Studium\PhD\Data\rawFiles\';

% Subfolder to Folder_raw... Note that this script handles *.raw data
% stored according to the following directory organisation:
%RAWData/Subject#/session
SubFolder={'SWAB_KP1' 'SWAB_KP2' 'SWAB_KP3' 'SWAB_KP4' 'SWAB_KP5' 'SWAB_KP6'};

% Subfolder of Folder_raw/Subfolder: can be either baseline 'bs' or
% stimulation 'stim'
Session={'bs' 'stim'};

% Handle for dataname
sub={'SWAB'};

% Get's all FFT files within dir 'Folder'
FFT_File=dir([Folder,'*FFT.mat']);    

% Saves the result into the same Folder
saveFolder=Folder;

%% Data Processing

% Allways check that the variable boundaries of 'l' and 'i' are within the
% correct range. If one *.mat file or *.raw file is damadged this code
% might crash... The error has to be fixed in the respective file or the
% corresponding datasets have to be excluded from analysis... If exclusion
% is the method of choise one might either (1) delete the dataset within
% the folder or (2) alter 'l=1:length(Session)' to skip the dataset. The
% later one is recommended in general.


for s=1:length(SubFolder)

     if sum(FFT_File(s).name(1:4)==sub{1})==4
         loadfile=[Folder,FFT_File(s).name];
         load(loadfile,'StimCh');
             
         for l=1:length(Session)
             Rawname=dir([Folder_raw,SubFolder{s},'\',Session{2},'\','*.raw']); 
             numfile=length(Rawname);
             tPos_all=[]; data=[]; samp=0;
             

                for i=1:numfile
                    display(i)
                    filenameraw=[Folder_raw,SubFolder{s},'\',Session{2},'\',Rawname(i).name];
                    
                    data_ref1 = loadEGIBigRaw(filenameraw,49);
                    data_ref2 = loadEGIBigRaw(filenameraw,56);
                    currentsamp=length(data_ref1);
                    
                   
                    data_ch = loadEGIBigRaw(filenameraw,StimCh);  
                    data_ref=data_ch-((data_ref1+data_ref2)/2);
                    data_ref_fil=filter(Hd_but1,data_ref);
                    clear data_ch data_ref
                    
                    data=[data data_ref_fil];
                    clear data_ref_fil
                    
                    
                    if sum(FFT_File(s).name(1:8)==Rawname(1).name(4:11))==8
                         eval('tPos=egiGetTriggersCorr(filenameraw,trigch);','tPos=[]') 
                         tPos_row=tPos+samp;
                         tPos_all=[tPos_all,tPos_row];
                         samp=samp+currentsamp;
                         clear tPos tPos_row
                    end
                    clear data_ref1 data_ref2 filenameraw currentsamp
                    
                end
                
               if ~isempty(tPos_all)
                    save([saveFolder,Rawname(1).name(4:11),'_FiltData_allnight.mat'],'data','StimCh','fs','refCh','Hd_but1','tPos_all')
               else
                 save([saveFolder,Rawname(1).name(4:11),'_FiltData_allnight.mat'],'data','StimCh','fs','refCh','Hd_but1')
               end
               
               clear Rawname numfile  tPos_all data samp    
         end
         
     else
             error('wrong Sub matching') 
     end
     clear StimCh
end
         