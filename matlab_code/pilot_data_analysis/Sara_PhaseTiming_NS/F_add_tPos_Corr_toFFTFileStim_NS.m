%% F_add_tPos_Corr_toFFTFileStim_NS

% Adds 'tPos_allCor' from '*FiltData_allnight.mat' generated in 'E' to the corresponding FFT.mat files

%% 
clear; close;

% '*FiltData_allnight.mat' and '*FFT.mat' should be in the same folder:
Folder='D:\Niklas\TestData2\SWAB(Pilot)\data\EGI\Filtered and analysed files\long mat files\';

% Session can be either 'stim' or 'bs'
Session={'stim\'};


for l=1:length(Session)
     SubSess_tPos=dir([Folder,Session{1},'*FiltData_allnight.mat']);
     Sub_FFT=dir([Folder,Session{1},'*FFT.mat']);
    
    for s=2:length(Sub_FFT)
        if sum(SubSess_tPos(s).name(1:8)==Sub_FFT(s).name(1:8))==8
            load([Folder,Session{1},SubSess_tPos(s).name],'tPos_allCor')
            save([Folder,Session{1},Sub_FFT(s).name],'tPos_allCor','-append')
            clear tPos_allCor
        else
            error('wrong sub match')
        end
    end        
end