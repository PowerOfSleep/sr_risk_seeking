# README #

### What is this repository for? ###

This repository represents a brought overview about study arm 2 of the TMS-SDSR study started in 2103 by Angelina Maric.
Study arm 2 is supposed to start in the first quarter of 2018 and is supposed to be a directly follow up of study arm 1 that was published in SLEEP:

__Intraindividual Increase of Homeostatic Sleep Pressure Across Acute and Chronic Sleep Loss: A High-Density EEG Study__,  Sleep, Volume 40, Issue 9, 1 September 2017, zsx122, https://doi.org/10.1093/sleep/zsx122

Additional data/info is available on the server:

`\\fl-daten\NOS_Forschungen\NOS-Schlaflabor\Forschung\Studien_aktuell\TMS-SDSR-2013\part2`

Note that this usz server is not directly accessible through the Kispi network!

### How do I get set up? ###

* New to git/bitbucket or repositories? Puhh... Come talk to Niklas Schneider

Other users:

* Windows/MAC/Linux users: Install [git bash](https://git-scm.com/downloads) on your maschine and set up and clone the repository.
  Solaris users: keep praying, I'm not implementing that stuff... Lord have mercy to your soul... 


* Organisatory files (eg. time sheets) will be stored here: `sr_risk_seeking/study_arm_2/organisation/` 

* Matlab code for training can be found here : `sr_risk_seeking/study_arm_2/matlab_code/training/`

* Matlab code for real data analysis follows soon

### Contribution guidelines ###

* Writing rights will only granted to users directly working on that project, especially on data analysis side
* Reading rights are free for all interested users

### Bug Fixes ###

* Users experiencing bugs are wellcome to submit there problem via the internal issue tracker... please state the error (screenshots are allways nice) or copy the error message...
  If you know which part of the code creates the error please also state that... please do not contact one of the ADMINs directly per mail

* If you feel comfortable with creating magic bugfixes in matlab yourself please feel free to fix the bug yourself.
  **BUT!!: Please make sure that it works before you push a commit or even better, create a new branch first and then send one of the ADMINs a merge request**

### Who do I talk to? ###

Person | E-Mail
-------|-------
Niklas Schneider (Admin) | niklas.schneider@usz.ch
Angelina Maric | angelina.maric@usz.ch
