%PVT_DataAna_TMS_Version1.2_M�rz2014.m
% Program to read in PVT files from subjects over all study parts.
% And to calculate RT, lapses and other parameters per patient and session
% And to create a bar plot for lapses, false starts and reaction time
% Current Directory einstellen: \\Fl-daten\nos_forschungen\NOS-Schlaflabor\Forschung\Studien_aktuell\TMS-SDSR-2013\Esther
% 11.M�rz2014: Fehler bei RT500-Parametern entdeckt (<100ms wurden nicht als NaN ersetzt bei RT500 > Programm korrigiert
% 18.M�rz 2015 AM: Erg�nzt um Variable speed und nansem f�r RT & RT500, zudem
% automatisierter Export aller Variabeln in Excel Output file benannt nach
% VP: daher immer alle Files f�r gleiche VP auf einmal verarbeiten 
% 2.4.15 AM speedRT500 hinzugef�gt


clear all
close all
alldata=[];

%%%Adjust:
datapath='F:\SWA_Boost_Pilot\PVT_analysis\';
datasheet='PVT_Outputfile_SWAB170321.xlsx'; %output file
datasheet_Lapses='PVT_Lapses_SWAB.xls'; %output file
datasheet_SpeedRT500='PVT_SpeedRT500_SWAB.xls'; %output file
datasheet_Speed='PVT_Speed_SWAB.xls'; %output file
datasheet_Perc10='PVT_Perc10_SWAB.xls'; %output file
datasheet_Perc90min10='PVT_Perc90min10_SWAB.xls'; %output file
PVTFileList='PVT_FileList_SWAB.xls';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cd(datapath)
% open excel file 'PVT_FileList_TMS' with info about path, pvtfilenames, studyday , testtime , and
% studypart  und study block sequence 
[NUMERIC,TXT]...
=xlsread(PVTFileList, -1);
% xlsread creates two types of data (numeric data and text data organized in cells
% -1: opens the excel file to select the patient to analyse in the FileList

% number of PVT files (=number of lines in xls file)
a=size(TXT);
pvtflcount=a(1);

% Abarbeiten der PVT_FileList
for k=1:pvtflcount
    %k=2;
    path=(TXT{k,1});
    fname=(TXT{k,2});
        filnm=[path fname];
    Subject=str2num(TXT{k,3});
    StudyDay=str2num(TXT{k,4});
    TestTimes=str2num(TXT{k,5});
    StudyPart=str2num(TXT{k,6});
    StudyBlockSequence=str2num(TXT{k,7});
    
    totblock=length(TestTimes);    % amount of tests in one pvt file

    fid = fopen(filnm, 'r');
    
    % Berechnung der verschiedensten Parameter pro einzelnen PVT-Test
    for part=1:totblock
        %part=1;
        skip=textscan(fid, 'N',1); % erste Linie muss �berlesen werden
        data = textscan(fid, '%f %f', 'headerlines', 15, 'delimiter', ',');
        %textscan stoppt nach Block da Leerlinie nicht erkannt wird
        
        RT=data{1}; RT=RT(2:(length(RT))-1);  %2, da erste Linie keine RT ist; -1, da am Ende des Tests immer 0 steht.
        CT=data{2}; CT=CT(2:(length(CT))-1);
        
        %Vorbereitung von Vektoren f�r weitere Berechnungen
        %RT500=RT;  % Vector RT ohne Werte die l�nger als 500ms sind
        Falsestart=zeros(length(RT),1); % macht Vector mit Nullen von gleicher L�nge wie Vector RT
        Lapse=zeros(length(RT),1);      % dito
        
        %unphysiological RTs<100 and error of commission (false starts)
        i=find(RT<100);
        RT(i)=NaN;  %hier muss noch GoodRT berechnet werden
        CT(i)=NaN;
        Falsestart(i)=1; %ersetzt 0 mit 1 an der Stelle wo RT<100ms war
        plot(CT,RT); %zur Kontrolle ob Programm richtig arbeitet (check mit Werten im PVT-File
        
        %Determine lapses f�r 500ms
        RT500=RT;  % Vectorvorbereitung f�r RT ohne Werte >500ms und <100ms (<100ms sind schon als NaN drin)
        i=find(RT>500);
        RT500(i)=NaN;
        Lapse(i)=1; %ersetzt 0 mit 1 an der Stelle wo RT>500ms war
        
        %Speed
        speed=RT;
        for n=1:length(RT);
        speed(n)=1/RT(n)*1000;
        end
        
        %Speed RT500
        speedRT500=RT500;
        for m=1:length(RT500)
            speedRT500(m)=1/RT500(m)*1000;
        end
        
        %Berechnung Lapses etc.
        totReactions=length(RT); % total Reaktionen pro PVT-Test (oft ca. 100)
        GoodRT=(length(RT)-nansum(Falsestart));
        Falsestarts=nansum(Falsestart);        
        Lapses=nansum(Lapse);
        totErrors=Lapses + Falsestarts;
        pFalsestarts=(Falsestarts*100/GoodRT); % prozentuale Fehlerrate relative zu den 'guten/richtigen' Reaktionen
        pLapses=Lapses*100/GoodRT;             % prozentuale Fehlerrate der Lapses relative zu den 'guten/richtigen' Reaktionen
        ptotErrors=totErrors*100/totReactions;           % prozentuale Fehlerrate relative zu allen Reaktionen
        
%         alldata=[alldata; Subject, StudyPart, StudyDay(part), TestTimes(part), StudyBlockSequence,...
%             totReactions, GoodRT, Lapses, pLapses, Falsestarts, pFalsestarts, totErrors, ptotErrors,...
%             nanmean(RT), nanstd(RT), nanvar(RT), nansem(RT)...
%             prctile(RT,50), prctile(RT,10), prctile(RT,90), (prctile(RT,90)-prctile(RT,10)),...
%             nanmean(RT500), nanstd(RT500), nanvar(RT500), nansem(RT500) ...
%             prctile(RT500,50), prctile(RT500,10), prctile(RT500,90), (prctile(RT500,90)-prctile(RT500,10)),...
%             nanmean(speed), nanstd(speed), nanvar(speed), nansem(speed) ...
%             prctile(speed,50), prctile(speed,10), prctile(speed,90), (prctile(speed,90)-prctile(speed,10)),...
%             nanmean(speedRT500), nanstd(speedRT500), nanvar(speedRT500), nansem(speedRT500) ...
%             prctile(speedRT500,50), prctile(speedRT500,10), prctile(speedRT500,90), (prctile(speedRT500,90)-prctile(speedRT500,10))];

%create celss containing variable of interest
% Lapses_Out = {'KP','Studientag','Order', 'TestTime', 'Lapses';Subject, StudyPart,StudyBlockSequence, TestTimes, Lapses };
% SpeedRT500_Out = {'KP','Studientag','Order', 'TestTime', 'SpeedRT500';Subject, StudyPart, StudyBlockSequence, TestTimes, nanmean(speedRT500) };
% Speed_Out = {'KP','Studientag','Order', 'TestTime', 'Speed';Subject, StudyPart,StudyBlockSequence, TestTimes, nanmean(speed) };
% Perc10_Out = {'KP','Studientag','Order', 'TestTime', 'Perc10';Subject, StudyPart,StudyBlockSequence, TestTimes, prctile(speed,10) };
% Perc90min10_Out = {'KP','Studientag','Order', 'TestTime', 'Perc10';Subject, StudyPart,StudyBlockSequence, TestTimes, (prctile(speed,90)-prctile(speed,10)) };
if StudyPart==1
    Cond='Stim'
elseif StudyPart==0
        Cond='BL'
end

% eval(['Lapses_' num2str(TestTimes) '_' Cond '=Lapses;'])
% eval(['SpeedRT500_' num2str(TestTimes) '_' Cond '=nanmean(speedRT500);'])
% eval(['Speed_' num2str(TestTimes) '_' Cond '=nanmean(speed);'])
eval(['Perc10_' num2str(TestTimes) '_' Cond '=prctile(speed,90);'])
% eval(['Perc90min10_' num2str(TestTimes) '_' Cond '=(prctile(speed,90)-prctile(speed,10));'])
%export data to excel file with all already processed data
% cd(datapath);
% allready_filled_pos=xlsread(datasheet_Lapses);
% pos=size(allready_filled_pos,1);%how many rows are already filled in the file
% position = pos+2; %bcs first row is ignored if there are only strings
% position=num2str(position);
% positionname=['A', position]; %that's where new data will be filled in the file
% 
% % warning('off','MATLAB:xlswrite:AddSheet');
% xlswrite(datasheet_Lapses,Lapses_Out(2,:),1,positionname);


    end
    status = fclose(fid);    
end
% Lapses_Out = {'KP','Order','Lapses_BL1','Lapses_BL2','Lapses_BL3',...
%     'Lapses_BL4','Lapses_Stim1','Lapses_Stim2','Lapses_Stim3','Lapses_Stim4';...
%     Subject, StudyBlockSequence, Lapses_1_BL,Lapses_2_BL,Lapses_3_BL,Lapses_4_BL,...
%     Lapses_1_Stim,Lapses_2_Stim,Lapses_3_Stim,Lapses_4_Stim};
% SpeedRT500_Out = {'KP','Order','SpeedRT500_BL1','SpeedRT500_BL2','SpeedRT500_BL3',...
%     'SpeedRT500_BL4','SpeedRT500_Stim1','SpeedRT500_Stim2','SpeedRT500_Stim3','SpeedRT500_Stim4';...
%     Subject, StudyBlockSequence, SpeedRT500_1_BL,SpeedRT500_2_BL,SpeedRT500_3_BL,SpeedRT500_4_BL,...
%     SpeedRT500_1_Stim,SpeedRT500_2_Stim,SpeedRT500_3_Stim,SpeedRT500_4_Stim};
% Speed_Out = {'KP','Order','Speed_BL1','Speed_BL2','Speed_BL3',...
%     'Speed_BL4','Speed_Stim1','Speed_Stim2','Speed_Stim3','Speed_Stim4';...
%     Subject, StudyBlockSequence, Speed_1_BL,Speed_2_BL,Speed_3_BL,Speed_4_BL,...
%     Speed_1_Stim,Speed_2_Stim,Speed_3_Stim,Speed_4_Stim};
Perc10_Out = {'KP','Order','Perc10_BL1','Perc10_BL2','Perc10_BL3',...
    'Perc10_BL4','Perc10_Stim1','Perc10_Stim2','Perc10_Stim3','Perc10_Stim4';...
    Subject, StudyBlockSequence, Perc10_1_BL,Perc10_2_BL,Perc10_3_BL,Perc10_4_BL,...
    Perc10_1_Stim,Perc10_2_Stim,Perc10_3_Stim,Perc10_4_Stim};
% Perc90min10_Out = {'KP','Order','Perc90min10_BL1','Perc90min10_BL2','Perc90min10_BL3',...
%     'Perc90min10_BL4','Perc90min10_Stim1','Perc90min10_Stim2','Perc90min10_Stim3','Perc90min10_Stim4';...
%     Subject, StudyBlockSequence, Perc90min10_1_BL,Perc90min10_2_BL,Perc90min10_3_BL,Perc90min10_4_BL,...
%     Perc90min10_1_Stim,Perc90min10_2_Stim,Perc90min10_3_Stim,Perc90min10_4_Stim};


%export data to excel file with all already processed data
% cd(datapath);
% allready_filled_pos=xlsread(datasheet_Lapses);
% pos=size(allready_filled_pos,1);%how many rows are already filled in the file
% position = pos+2; %bcs first row is ignored if there are only strings
% position=num2str(position);
% positionname=['A', position]; %that's where new data will be filled in the file
% 
% % warning('off','MATLAB:xlswrite:AddSheet');
% xlswrite(datasheet_Lapses,Lapses_Out(2,:),1,positionname);
% cd(datapath);
% allready_filled_pos=xlsread(datasheet_SpeedRT500);
% pos=size(allready_filled_pos,1);%how many rows are already filled in the file
% position = pos+2; %bcs first row is ignored if there are only strings
% position=num2str(position);
% positionname=['A', position]; %that's where new data will be filled in the file
% 
% % warning('off','MATLAB:xlswrite:AddSheet');
% xlswrite(datasheet_SpeedRT500,SpeedRT500_Out(2,:),1,positionname);
% 
% cd(datapath);
% allready_filled_pos=xlsread(datasheet_Speed);
% pos=size(allready_filled_pos,1);%how many rows are already filled in the file
% position = pos+2; %bcs first row is ignored if there are only strings
% position=num2str(position);
% positionname=['A', position]; %that's where new data will be filled in the file
% 
% % warning('off','MATLAB:xlswrite:AddSheet');
% xlswrite(datasheet_Speed,Speed_Out(2,:),1,positionname);

cd(datapath);
allready_filled_pos=xlsread(datasheet_Perc10);
pos=size(allready_filled_pos,1);%how many rows are already filled in the file
position = pos+2; %bcs first row is ignored if there are only strings
position=num2str(position);
positionname=['A', position]; %that's where new data will be filled in the file

% warning('off','MATLAB:xlswrite:AddSheet');
xlswrite(datasheet_Perc10,Perc10_Out(2,:),1,positionname);

% cd(datapath);
% allready_filled_pos=xlsread(datasheet_Perc90min10);
% pos=size(allready_filled_pos,1);%how many rows are already filled in the file
% position = pos+2; %bcs first row is ignored if there are only strings
% position=num2str(position);
% positionname=['A', position]; %that's where new data will be filled in the file
% 
% warning('off','MATLAB:xlswrite:AddSheet');
% xlswrite(datasheet_Perc90min10,Perc90min10_Out(2,:),1,positionname);
% 
% SpeedRT500_Out = {'KP','Studientag','Order', 'TestTime', 'SpeedRT500';Subject, StudyPart, StudyBlockSequence, TestTimes, nanmean(speedRT500) };
% Speed_Out = {'KP','Studientag','Order', 'TestTime', 'Speed';Subject, StudyPart,StudyBlockSequence, TestTimes, nanmean(speed) };
% Perc10_Out = {'KP','Studientag','Order', 'TestTime', 'Perc10';Subject, StudyPart,StudyBlockSequence, TestTimes, prctile(speed,10) };
% Perc90min10_Out = {'KP','Studientag','Order', 'TestTime', 'Perc10';Subject, StudyPart,StudyBlockSequence, TestTimes, (prctile(speed,90)-prctile(speed,10)) };

% write alldata into excel output sheet
% sheet=['VP_',num2str(Subject)]
% cd(datapath);
% xlswrite(datasheet,alldata,sheet);

% %% Figuren
% %%%%%%%%% muss je nach Figur welche berechnet werden soll angepasst werden !!!!!
% %Figur (bar plot for lapses)
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% xlab=num2str(alldata(1:size(alldata,1),4));   %Achtung anpassen der anzahl linien (1:xx,4)
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Create figures
% 
% for i = 1:3
% 
%     switch i
%         case 1
%             figure1 = figure('PaperSize',[20.98 29.68]);
%             CurrentFigure = figure1;
%         case 2
%             figure2 = figure('PaperSize',[20.98 29.68]);
%             CurrentFigure = figure2;
%         case 3
%             figure3 = figure('PaperSize',[20.98 29.68]);
%             CurrentFigure = figure3;
% 
%     end
% 
% 
% % Create axes
% axes('Parent',CurrentFigure,...
%     'XTickLabel',xlab,...
%     'XTick',(1:1:length(xlab)),...
%     'TickDir','out',...
%     'Position',[0.13 0.2552 0.775 0.6698]);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hold('all');
% % Create bar
% 
%  switch i
%      case 1
%         bar(alldata(1:size(alldata,1),8)); %Lapses   %Achtung anpassen der anzahl linien
%         %(1:xx,4)
% 
%         hold('all');
%         xlabel('Time of day');
%         ylabel('Lapses');
%         title(['Lapses of VP ',num2str(Subject)]);
%         
%         hold;
%         
%      case 2
%     
%             bar(alldata(1:size(alldata,1),10));  %False starts %Achtung anpassen der anzahl linien (1:xx,4)
% 
%             hold('all');
%             xlabel('Time of day');
%             ylabel('False Starts');
%             title(['False Starts of VP ',num2str(Subject)]);
% 
%      case 3
% 
% %
%             bar(alldata(1:size(alldata,1),21));  %RT500  %Achtung anpassen der anzahl linien
% %(1:xx,4)
% 
%             hold('all');
%             xlabel('Time of day');
%             ylabel('RT500 [ms]');
%             title(['RT (>500ms excluded) of VP ',num2str(Subject)]);
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  end
% % Create lines
% annotation(CurrentFigure,'line',[0.2717 0.2717],[0.1085 0.9167]);
% annotation(CurrentFigure,'line',[0.5045 0.5045],[0.1014 0.919]);
% 
% % Create textbox
% annotation(CurrentFigure,'textbox','String',{'BL'},...
%     'Position',[0.1378 0.1107 0.1224 0.07216]);
% annotation(CurrentFigure,'textbox','String',{'SR'},...
%     'Position',[0.2872 0.1081 0.04977 0.07216]);
% annotation(CurrentFigure,'textbox','String',{'SD'},...
%     'Position',[0.5149 0.1097 0.04977 0.07216]);
% end
